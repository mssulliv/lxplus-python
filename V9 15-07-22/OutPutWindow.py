import time, os

from PyQt6.QtWidgets import QPlainTextEdit, QTabWidget,QTextEdit, QLabel, QApplication, QWidget, QPushButton, QGridLayout, QFileDialog, QGroupBox, QHBoxLayout, QVBoxLayout
import sys
from PyQt6.QtGui import QTextCursor
from PyQt6.QtWidgets import QLineEdit, QTextBrowser, QComboBox, QErrorMessage, QMainWindow,QTableWidget,QTableWidgetItem, QCheckBox
from PyQt6 import QtCore
from PyQt6.QtCore import QRunnable, pyqtSlot, QThreadPool
from random import randint


class AnotherWindow(QWidget):
    """
    This "window" is a QWidget. If it has no parent, it
    will appear as a free-floating window as we want.
    """
    def __init__(self, UniqueJobTrackListSLURM, username):
        super().__init__()
        print("opening output window")
        # Need to extract the Job ID's from the passed JobTackList
        self.TabJobIDs = []
        for i in UniqueJobTrackListSLURM:
            self.TabJobIDs.append(i[0])
        print("outwind 1")
        layout = QGridLayout()
        self.setWindowTitle('CST Simulation Outputs')
        self.setLayout(layout)
        self.username = username
        print("outwind 2")

        # We need to keep the Tab and Messages objs in a list so we can access them easily to edit them
        self.TabList = []
        self.TabMessagesList = []
        self.tabwidget = QTabWidget()
        print("outwind 3")

        #self.AddJobTab("Tab 1")
        print("outwind 4")
        #self.AddJobTab("Tab 2")

        self.NumTabsIndex = 0
        print("self.TabJobIDs:", self.TabJobIDs)
        for k in self.TabJobIDs:
            self.AddJobTab(k)
        print("outwind 5")
        #self.TabMessagesList[1].append("Hello there")

        layout.addWidget(self.tabwidget, 0, 0)
        print("outwind 6")
        #self.PrintSLURMOutput()

    def AddJobTab(self, JobID):
        print("AddJobTab1")
        self.TabList.append(QWidget())
        tab1 = self.TabList[self.NumTabsIndex]
        print("AddJobTab2")

        tab1.layout = QVBoxLayout()
        self.tabwidget.addTab(tab1, JobID)
        print("AddJobTab3")

        #self.TabMessagesList.append(QTextBrowser())
        self.TabMessagesList.append(QPlainTextEdit())
        TabMessages = self.TabMessagesList[self.NumTabsIndex]
        tab1.layout.addWidget(TabMessages)
        tab1.setLayout(tab1.layout)
        print("AddJobTab4")

        #TabMessages.append("This is the output for Job: " + JobID)
        print("AddJobTab5")
        self.NumTabsIndex = self.NumTabsIndex + 1

    def PrintSLURMOutput(self):
        print("printing slurm output1")
        NumTabsIndex = 0
        for i in self.TabJobIDs:
            # to stop from printing the file contents one after the other
            # clear the text in the message box and then reprint the file contents
            print("printing slurm output1.1")
            print("NumTabsIndex:", NumTabsIndex)
            #self.TabMessagesList[NumTabsIndex].clearHistory()
            print("printing slurm output2")
            print("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/slurm-"+i+".out")
            # If the CST Sim has not yet started running then the output file will not exist and the code will crash
            # i.e. we need to check that the output file exists first!
            if os.path.isfile("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/slurm-"+i+".out"):
                with open("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/slurm-"+i+".out",'r') as outfile:
                    print("printing slurm output3")
                    #self.TabMessagesList[NumTabsIndex].append("=====================")
                    print("printing slurm output3.1")
                    temptext = outfile.read()
                    print(temptext[-50:])
                    print("printing slurm output3.2")
                    #self.TabMessagesList[NumTabsIndex].clear()
                    print("printing slurm output3.3")
                    time.sleep(2)
                    print("printing slurm output3.4")
                    #self.TabMessagesList[NumTabsIndex].clear()
                    print("printing slurm output3.5")
                    time.sleep(2)
                    print("printing slurm output3.6")
                    self.TabMessagesList[NumTabsIndex].setPlainText(temptext)
                    print("printing slurm output4")

                    NumTabsIndex = NumTabsIndex + 1
                    print("printing slurm output6")
