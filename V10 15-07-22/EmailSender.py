import smtplib, ssl
from email.message import EmailMessage

class Emailer:
    def __init__(self, email):
        self.message = ""
        self.port = 465  # For SSL
        # Create a secure SSL context
        self.context = ssl.create_default_context()

        self.sender_email = "CERN.HPC.Cluster.Sender@gmail.com"
        self.password = 'pythontesting'
        self.receiver_email = email
        self.msg = EmailMessage()

    def SendMessage(self, message1, subject):
        self.msg.set_content(message1)
        self.msg['Subject'] = subject
        self.msg['From'] = self.sender_email
        self.msg['To'] = self.receiver_email

        with smtplib.SMTP_SSL("smtp.gmail.com", self.port, context=self.context) as server:
            server.login(self.sender_email, self.password)
            server.send_message(self.msg)



#test = Emailer('michael.stephen.sullivan@cern.ch')
#test.SendMessage("This is a test using EmailMessage!", "Testing More")


