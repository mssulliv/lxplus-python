import Make_Scripts, OpenPutty

ScriptsObj = Make_Scripts.scripts(r"C:\Users\mssulliv\cernbox\Documents\HTCondor_Test_files\Setup.yaml")
ScriptsObj.loadyaml()
ScriptsObj.MakeSubFile()
ScriptsObj.MakeCSTScrpt()

# Here are the details that LXPLUX needs to sign in and connect
host = "lxplus"
port = 22
initial = ""
username = ""
password = ""

connection = OpenPutty.ssh(host, username, password,ScriptsObj)
connection.execute_scripts()