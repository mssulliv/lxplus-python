import os
from tabulate import tabulate
from PyQt6.QtWidgets import QLabel, QApplication, QWidget, QPushButton, QGridLayout, QFileDialog, QGroupBox, QHBoxLayout, QVBoxLayout,QTabWidget,QPlainTextEdit,QListWidget,QListWidgetItem,QMessageBox
import sys
from PyQt6.QtGui import QIcon,QDrag
from PyQt6.QtWidgets import QProgressBar, QLineEdit, QTextBrowser, QComboBox, QErrorMessage, QMainWindow,QTableWidget,QTableWidgetItem, QCheckBox, QStyleFactory
#from PyQt6 import QtCore
from PyQt6.QtCore import QRunnable, pyqtSlot, QThreadPool, Qt, QUrl,QMimeData, QObject, pyqtSignal,QRect
import Make_Scripts, OpenPutty, time, EmailSender,Make_MPI_Script
from datetime import datetime, timedelta
#from collections import OrderedDict
import yaml,traceback
#from waiting import wait
#import OutPutWindow

DEFAULT_STYLE = '''
QProgressBar{
    text-align: center;
}
QProgressBar::chunk {
    background-color: #F44336;
    color: #F44336;
}
'''

COMPLETED_STYLE  = '''
QProgressBar{
    text-align: center;
}
QProgressBar::chunk {;
    background-color: #00FF00;
}
'''

class JobTableWindow(QWidget):
    """
    This "window" is a QWidget. If it has no parent,
    it will appear as a free-floating window.
    """
    closingsignal = pyqtSignal()
    def __init__(self):
        super().__init__()
        # Here we initiate the different areas on the GUI window
        self.setWindowTitle('LXPLUS CST Submission Job Window')
        self.setGeometry(600, 400, 1000, 400)
        self.JobTableArea()



        # Add the Box widgets to the window
        self.gridAll = QGridLayout()
        self.gridAll.addWidget(self.JobTableBox, 0,0)

        self.setLayout(self.gridAll)



    def closeEvent(self, event):
        self.closingsignal.emit()
        print('Table window is closing')
        QWidget.closeEvent(self, event)

    def JobTableArea(self):
        # This function defines the area where the Job Table is displayed in the GUI
        self.JobTableBox = QGroupBox("Job Status")
        #self.JobTableBox.setGeometry(QtCore.QRect(20, 300, 511, 91))
        self.JobTableBox.setMinimumSize(600,400)
        self.JobTableBox.setGeometry(0, 0, 4000, 2000)
        self.JobStatusLabel0 = QLabel()
        self.JobStatusLabel0.setText("Status: ")
        self.JobStatusLabel = QLabel()
        self.JobStatusLabel.setText("Idle")
        self.JobTable = QTableWidget()
        #self.JobTable.setGeometry(QtCore.QRect(10, 90, 331, 111))
        self.JobTable.setGeometry(0, 0, 4000, 2000)

        self.JobTable.setRowCount(20)
        self.JobTable.setColumnCount(9)

        self.JobTable.setHorizontalHeaderLabels(['Peek!','Job ID', 'Name','Submission (Date - Time)','Runtime' , 'State', 'Server', 'Partition', 'Nodes'])
        self.JobTable.horizontalHeader().setDefaultSectionSize(100)

        self.PeekButtons = []
        for index in range(self.JobTable.rowCount()):
            self.buttonTest = QPushButton("Output Peek")
            self.PeekButtons.append(self.buttonTest)
            self.JobTable.setCellWidget(index, 0, self.PeekButtons[index])
            self.PeekButtons[index].setDisabled(True)
            self.PeekButtons[index].clicked.connect(self.handleButtonClicked)


        TableGrid= QGridLayout()
        TableGrid.addWidget(self.JobStatusLabel, 0, 1)
        TableGrid.addWidget(self.JobStatusLabel0, 0, 0)
        TableGrid.addWidget(self.JobTable,1,0,20,10)
        #TableGrid.addWidget(self.ShowOutputButton, 1, 0)
        self.JobTableBox.setLayout(TableGrid)

    def PrintTableWindow(self, UniqueJobTrackListCondor,UniqueJobTrackListSLURM):
        self.UniqueJobTrackListCondor = UniqueJobTrackListCondor
        self.UniqueJobTrackListSLURM = UniqueJobTrackListSLURM



        print("=========== PrintTableWindow ===========")
        print("self.UniqueJobTrackListCondor",self.UniqueJobTrackListCondor)
        print("UniqueJobTrackListSLURM",self.UniqueJobTrackListSLURM)

        print('Made Buttons')
        # Print Condor (LXPLUS) Jobs
        for i in range(len(self.UniqueJobTrackListCondor)):
            for j in range(len(self.UniqueJobTrackListCondor[i])):
                self.JobTable.setItem(i, j+1, QTableWidgetItem(self.UniqueJobTrackListCondor[i][j]))
            if self.UniqueJobTrackListCondor[i][4] == 'Running':
                self.PeekButtons[i].setDisabled(False)
        print("Updated Condor Table!")
        # Print SLURM Jobs
        for i in range(len(self.UniqueJobTrackListSLURM)):
            for j in range(len(self.UniqueJobTrackListSLURM[i])):
                # print("i", i)
                # print("j", j)
                self.JobTable.setItem(i + len(self.UniqueJobTrackListCondor), j+1,
                                      QTableWidgetItem(self.UniqueJobTrackListSLURM[i][j]))
            if self.UniqueJobTrackListSLURM[i][4] == 'Running':
                self.PeekButtons[i+len(self.UniqueJobTrackListCondor)].setDisabled(False)
        print("Updated SLURM Table!")
        SubmitTime = datetime.now()
        current_time = SubmitTime.strftime("%H:%M:%S")
        self.JobStatusLabel.setText("Last Updated: " + current_time)

    def handleButtonClicked(self):
        button = QApplication.focusWidget()
        # or button = self.sender()
        index = self.JobTable.indexAt(button.pos())
        if index.isValid():
            print(index.row(), index.column())
            print("Peek at: " + str(self.JobTable.item(index.row(), index.column()+1).text()))
            jobID = str(self.JobTable.item(index.row(), index.column()+1).text())
            server = str(self.JobTable.item(index.row(), index.column() + 6).text())
            print("jobID = ", jobID)
        self.outwindow = CondorTailWindow(jobID,server)
        self.outwindow.show()



class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        tuple (exctype, value, traceback.format_exc() )

    result
        object data returned from processing, anything

    progress
        int indicating % progress

    '''
    finished = pyqtSignal(int)
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)
    simpleerror = pyqtSignal(str)


class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()
        self.threadfinished = False

        # Add the callback to our kwargs
        #self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''

        # Retrieve args/kwargs here; and fire processing using them
        try:
            print("run1")
            result = self.fn(*self.args, **self.kwargs)
            print("run2")
        except:
            print("error emitted1!!")
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            if 'yaml' in traceback.format_exc(): print("yaml in traceback")
            #self.signals.error.emit((exctype, value, traceback.format_exc()))
            print("error emitted2!!")
            if 'yaml' in traceback.format_exc(): self.signals.simpleerror.emit("Issue with .yaml file, please check!")
        else:
            print("emit result")
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.threadfinished = True
            self.signals.finished.emit(1)  # Done
            print(f"emit finished for function: {self.fn}")

class ListBoxWidget(QListWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setAcceptDrops(True)
        self.resize(600, 600)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(Qt.DropAction.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(Qt.DropAction.CopyAction)
            event.accept()

            self.links = []
            for url in event.mimeData().urls():
                # https://doc.qt.io/qt-5/qurl.html
                if url.isLocalFile():
                    self.links.append(str(url.toLocalFile()))
                else:
                    self.links.append(str(url.toString()))
            self.addItems(self.links)
        else:
            event.ignore()

class Window(QWidget):

    def __init__(self):
        QWidget.__init__(self)
        # here we define various variables that will be used in the code
        self.SetupFileName = ''
        self.CSTFileName = ''
        self.loginCheck = 0
        self.ClusterID = ''
        self.JobFinishedCheck = 0
        self.JobExecutingCheck = 0
        self.ScriptsObj = None
        self.TableUpdateStarted = 0
        self.CodeBusyCheck = 0
        self.First_Update_Check = 0
        self.UserEmail = ''
        self.ServerChoice = ''
        self.EmailBeenSentCheckCondor = []
        self.EmailBeenSentCheckSLURM = []
        self.CancelledJobs = []
        self.OutputWindowOpenCheck = 0
        self.setWindowTitle('LXPLUS CST Submission')
        self.setWindowIcon(QIcon("CERN-Logo.png"))
        self.w = None  # No external window yet.
        self.NumTabsIndex = 0
        self.OldJobsHistoryReadCheck = 0
        #self.setFixedHeight(200)
        #self.setFixedWidth(200)

        self.w = JobTableWindow()

        self.w.show()

        #self.w2 = CondorTailWindow("14521")
        #self.w2.show()


        self.threadpool = QThreadPool()

        # Here we initiate the different areas on the GUI window
        self.setGeometry(500,300,1000,400)
        self.UserPassArea()
        self.SelectFileArea()
        self.MessagesArea()
        self.ActionsArea()
        self.OptionsArea()
        self.JobTableArea()
        self.CancelJobArea()
        #self.SLURMJobOutputArea()

        self.JobQueueArea()
        self.SubmittedJobsArea()

        # Add the Box widgets to the window
        self.gridAll = QGridLayout()
        self.gridAll.addWidget(self.UserPassBox,0,0)
        self.gridAll.addWidget(self.SelectFileBox, 1, 0)
        self.gridAll.addWidget(self.MessagesBox, 2, 0)
        self.gridAll.addWidget(self.CancelBox, 2, 1)
        self.gridAll.addWidget(self.ActionsBox, 1, 1)
        self.gridAll.addWidget(self.OptionsBox, 0, 1)
        self.gridAll.addWidget(self.QueueBox, 3, 0)
        self.gridAll.addWidget(self.SubmittedBox, 3, 1)

        # JobTableBox now opens in another window! This makes the GUI easier to have on screen
        #self.gridAll.addWidget(self.JobTableBox, 4, 0,1,2)
        #self.gridAll.addWidget(self.SLURMJobOutputBox, 3, 2, 1, 2)
        #self.gridAll.addWidget(self.CancelBox, 1, 2)
        #self.gridAll.setRowStretch(4, 2)
        #self.gridAll.setColumnStretch(4, 2)

        self.setLayout(self.gridAll)

        #Create the settings file when the window starts up
        self.SettingsFileDir = r"LXPLUS_SLURM_GUI_Settings.yaml"
        #assert os.path.isfile(self.SettingsFileDir)
        if not os.path.isfile(self.SettingsFileDir):
            self.SettingsFile = open(self.SettingsFileDir, 'w')
            self.SettingsFile.close()
        else:
            # If a Settings file already exists, lets read it in
            with open(self.SettingsFileDir, 'r') as ymlfile:
                self.LoadedSettings = yaml.safe_load(ymlfile)
                print(self.LoadedSettings)
            if not self.LoadedSettings == None:
                self.useredit.setText(self.LoadedSettings['Username'])
                self.passedit.setText(self.LoadedSettings['Password'])
                self.Emailedit.setText(self.LoadedSettings['Email'])
                self.Messages.append("Previous settings loaded.")
                self.RememberLoginCheck.setChecked(True)

        print("Settings File has been created in: ", os.getcwd())
        self.Messages.append("Settings File has been created in: " + os.getcwd())

        # Lets also create a file which will be used to store extra info about submitted jobs
        self.JobInfoFileDir = r"JobHistory.txt"
        self.Historytableread = []
        if not os.path.isfile(self.JobInfoFileDir):
            # The first line of the table will be used as the headers
            self.Historytableread.append(['JobId', 'Server', 'CST file','Time submitted'])
            self.JobInfoHistoryFile = open(self.JobInfoFileDir, 'w')
            self.JobInfoHistoryFile.write(tabulate(self.Historytableread, headers="firstrow",floatfmt="10.0f"))
            self.JobInfoHistoryFile.close()
            # We need to make sure that we append to this file after creation so the history is not overwritten!
        else:
            # If the History file already exists, then we need to read the file into a list, append to update it and
            # then rewrite the updated file
            self.JobInfoHistoryFile = open(self.JobInfoFileDir, 'r')
            table = self.JobInfoHistoryFile.read().split('\n')
            for i in table:
                # we read it in like this to make it easier to append to and the tabulate
                self.Historytableread.append(list(filter(None, i.split('  '))))
                #print("self.Historytableread",self.Historytableread)
            if not self.Historytableread == [[]]:
                self.Historytableread.pop(1) # this deletes the '--------' line from the read in history file

        print('self.Historytableread', self.Historytableread)



    #def show_new_window(self):
    #    while self.CodeBusyCheck == 1:
    #        # wait until the code isnt busy doing something else (to stop crashes)
    #        print("WAITING TO OPEN OUTPUT")
    #        time.sleep(1)
    #    if self.w is None:
    #        self.w = OutPutWindow.AnotherWindow(self.UniqueJobTrackListSLURM, self.username)
    #        print("show_new_window1")
    #    print("show_new_window2")
    #    self.w.show()
    #    self.w.PrintSLURMOutput()
    #    print("show_new_window3")

    def UserPassArea(self):
        # This area defines the box where the user enters their username and password
        self.UserPassBox = QGroupBox("Enter Username and Password")
        self.UserPassBox.setGeometry(QRect(20, 30, 211, 91))

        userlabel = QLabel('Username:')
        pwlabel = QLabel('Password:')
        self.useredit = QLineEdit()
        self.passedit = QLineEdit()
        self.passedit.setEchoMode(QLineEdit.EchoMode.Password)

        self.loginprogressBar = QProgressBar(self)
        self.loginprogressBar.setStyleSheet(DEFAULT_STYLE)
        self.loginprogressBar.setRange(0, 1)
        self.loginprogressBar.setMinimum(0)
        self.loginprogressBar.setMaximum(1)
        self.loginprogressBar.setValue(1)
        self.loginprogressBar.setFormat("Not logged in")
        self.loginprogressBar.setTextVisible(1)


        UPgrid = QGridLayout()
        UPgrid.addWidget(userlabel, 0, 0)
        UPgrid.addWidget(self.useredit, 0, 1)
        UPgrid.addWidget(pwlabel, 1, 0)
        UPgrid.addWidget(self.passedit, 1, 1)

        #This can be used if the user needs to give an email instead of using usual @cern email
        self.EmailLabel = QLabel("Send Email when Job Finished?:")
        self.Emailedit = QLineEdit()

        #UPgrid.addWidget(self.EmailLabel, 2, 0)
        #UPgrid.addWidget(self.Emailedit, 2, 1)

        UserLoginBtn = QPushButton('Login')
        UserLoginBtn.clicked.connect(self.UserAreaLogin)
        UPgrid.addWidget(UserLoginBtn, 4, 0)

        self.RememberLoginCheck = QCheckBox()
        self.RememberLoginCheck.setText("Remember my login details")

        UPgrid.addWidget(self.RememberLoginCheck , 3, 0)
        UPgrid.addWidget(self.loginprogressBar, 4, 1)

        self.UserPassBox.setLayout(UPgrid)

    def UserAreaLogin(self):
        # When the "login" button is clicked, this function is executed

        global username
        global password

        username = self.useredit.text()
        password = self.passedit.text()

        self.username = self.useredit.text()
        self.password = self.passedit.text()
        self.UserEmail = self.Emailedit.text()


        time.sleep(1)
        self.host = "lxplus"

        self.connectionTemp = OpenPutty.ssh(self.host, self.username, self.password, None)
        print(self.connectionTemp)
        if self.connectionTemp.loginFail == 1:
            print("Login attempt failed")
            self.Messages.append('Login attempt failed - Please try again!')
            self.loginprogressBar.setFormat('Login attempt failed - Please try again!')
            return
        print("test4")
        self.Messages.append('Login attempt successful!')
        self.loginCheck = 1
        self.loginprogressBar.setStyleSheet(COMPLETED_STYLE)
        self.loginprogressBar.setFormat("Logged in!")

        self.connectionTemp.open_shell()
        self.connectionTemp.send_shell('kinit ' + self.username)
        time.sleep(1)
        self.connectionTemp.send_shell(self.password)
        self.Messages.append('Kerboros Token requested.')
        time.sleep(1)
        #print(self.connectionTemp.strdata.split("\\n"))
        self.connectionTemp.close_connection()
        print("temp connection closed")
        print("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/")
        # create directory to store the SLURM output files for later reading
        print(os.path.isdir("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/"))
        if not os.path.isdir("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/"):
            os.mkdir("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/")

        # If the "Remember my login" checkbox is checked, lets save a "settings" file to the local PC so store this info
        if self.RememberLoginCheck.isChecked() == True:
            print("Saving login details to " + self.SettingsFileDir)
            self.Messages.append("Saving login details to " + self.SettingsFileDir)
            with open(self.SettingsFileDir, 'w') as file:
                file.write("Username: " + self.username + "\n")
                file.write("Password: " + self.password + "\n")
                file.write("Email: " + self.UserEmail + "\n")
        else:
            # User does not want settings saved so rewrite the settings file
            with open(self.SettingsFileDir, 'w') as file:
                file.close()


    def SelectFileArea(self):
        # This area defines the box where the user selects their setup and CST files
        self.SelectFileBox = QGroupBox("Select Files (single Job submission):")
        self.SelectFileBox.setGeometry(QRect(20, 30, 211, 91))

        self.SetupFileInfoLabel = QLabel('Here you can select a single .yaml file to submit or just create the scripts for checking.')

        self.SetupFileDialogButton = QPushButton('Setup File')
        self.SetupFileDialogButton.clicked.connect(self.SelectSetupFileButtonClick)

        self.SetupFileNameLabel = QLabel()

        self.SubmitButton = QPushButton('Submit Job')
        self.MakeScriptsOnlyButton = QPushButton('Make Scripts only')
        self.SubmitButton.clicked.connect(self.SubmitButtonClick)
        self.MakeScriptsOnlyButton.clicked.connect(self.MakeScriptsOnlyButtonClick)

        SFgrid = QGridLayout()

        SFgrid.addWidget(self.SetupFileInfoLabel, 0, 0,1,0)
        SFgrid.addWidget(self.SetupFileDialogButton, 1,0)
        SFgrid.addWidget(self.SetupFileNameLabel, 1, 1)
        SFgrid.addWidget(self.SubmitButton, 2, 0)
        SFgrid.addWidget(self.MakeScriptsOnlyButton, 2, 1)

        self.SelectFileBox.setLayout(SFgrid)

    def SelectSetupFileButtonClick(self):
        self.SetupFileName = QFileDialog.getOpenFileName(parent=self, directory=os.getcwd(),filter='*.yaml')[0]
        self.SetupFileNameLabel.setText('Setup file: ' + self.SetupFileName.split(r'/')[-1])
        print(self.SetupFileName)

    def MessagesArea(self):
        # This area defines the box where messages are outputted
        # we add text to the message box by self.Messages.append()
        self.MessagesBox = QGroupBox("Messages")
        self.SelectFileBox.setGeometry(QRect(20, 30, 211, 91))
        self.Messages = QTextBrowser(self.MessagesBox)
        self.Messages.setGeometry(QRect(10, 90, 331, 111))
        MessGrid= QGridLayout()
        MessGrid.addWidget(self.Messages,0,0)
        self.MessagesBox.setLayout(MessGrid)

    def JobQueueArea(self):
        # This area will allow the user to drag and drop files into a list (Queue) which will then be automatically
        # submitted
        self.QueueBox = QGroupBox("Job Queue")
        self.JobQueueAreaInfoLabel = QLabel()
        self.JobQueueAreaInfoLabelText = "Drag and drop your .yaml files below, then click \"Submit Job(s)\". \nOnce started " + \
            "the program will automatically submit jobs in the queue once the Job Table has finished updating." + \
            "\nTo remove Jobs, highlight them below and click \"Remove Job(s)\"" + \
            "\nFiles should have name: Setup_XXX_YYY.yaml \nXXX: can be anything \nYYY: Name of server (e.g. LXPLUS, SLURM)" + \
            "\nExample: Setup_ChamberTest_LXPLUS.yaml"
        self.JobQueueAreaInfoLabel.setText(self.JobQueueAreaInfoLabelText)

        self.listbox_view = ListBoxWidget(self)
        #self.listbox_view = ListBoxWidget(self)

        self.RemoveButton = QPushButton('Remove Job')
        self.RemoveButton.clicked.connect(self.RemoveButtonClick)

        self.QueueGoButton = QPushButton('Submit Job(s)')
        self.QueueGoButton.clicked.connect(self.JobQueueSubmit)

        QueueGrid = QGridLayout()
        QueueGrid.addWidget(self.JobQueueAreaInfoLabel, 0, 0)
        QueueGrid.addWidget(self.listbox_view, 1, 0, 1, 2)
        QueueGrid.addWidget(self.RemoveButton, 3, 0)
        QueueGrid.addWidget(self.QueueGoButton, 2, 0)
        self.QueueBox.setLayout(QueueGrid)

    def SubmittedJobsArea(self):
        # This area will allow the user to drag and drop files into a list (Queue) which will then be automatically
        # submitted
        self.SubmittedBox = QGroupBox("Submitted Jobs")
        self.SubmittedList = QListWidget()

        SubmittedJobsGrid = QGridLayout()
        SubmittedJobsGrid.addWidget(self.SubmittedList, 0, 0, 1, 2)
        self.SubmittedBox.setLayout(SubmittedJobsGrid)

    def RemoveButtonClick(self):
        self.RemoveButton.setDisabled()
        listItems = self.listbox_view.selectedItems()
        if not listItems: return
        for item in listItems:
            print("removed ", item.text(), " from Job Queue")
            self.listbox_view.takeItem(self.listbox_view.row(item))

    def JobQueueSubmit(self):
        print("starting")
        # Should check if user is logged in or code will crash!
        if self.loginCheck == 0:
            login_error_dialog = QErrorMessage(self)
            login_error_dialog.setWindowTitle("Error!")
            login_error_dialog.showMessage('Error: User has not logged in!')
            return

        self.JobStatusLabel.setText("Submitting Job(s) in Queue")
        #self.QueueGoButton.setDisabled(1)
        items = []
        for index in range(self.listbox_view.count()):
            items.append(self.listbox_view.item(index).text())
        print(items)
        # make a list of files which have been matched up in the format [[.cst, .yaml], [.cst, .yaml], ... ]
        self.matchedfileList = []
        # First we need to check that the files in the queue have the corresponding .yaml or .cst files
        for file in items:
            if ".yaml" in file:
                self.matchedfileList.append(self.matchedfileList.append(file))

        # Get rid of duplicates in the self.matchedfileList list of jobs
        self.UniquematchedfileList = []
        for i in self.matchedfileList:
            if i not in self.UniquematchedfileList:
                self.UniquematchedfileList.append(i)
        print("UniquematchedfileList: ", self.UniquematchedfileList)

        # Now need to submit the jobs in the queue
        self.itemindex = 0
        for job in items:
            self.moveon = 0

            ServerChoice = job.replace(".yaml",'').split("_")[-1]
            print(ServerChoice)
            print(f"============ Submitting Job: {job} ============" )

            if ServerChoice == 'LXPLUS':
                self.Messages.append('Submitting job to LXPLUS Cluster')
                print("done here0")
                worker = Worker(self.SubmitExecute, job)

                # simple error in the worker class sees "yaml" in traceback and passes "yaml error" to self.Error_Message
                worker.signals.simpleerror.connect(self.Error_Message)
                #worker.signals.finished.connect(self.job_submitted_check)
                self.threadpool.start(worker)

                #pool.start(worker)
            elif ServerChoice == 'SLURM':
                self.Messages.append('Submitting job to SLURM Cluster')
                workerSLURM = Worker(self.SubmitExecuteSLURM, job)
                self.threadpool.start(workerSLURM)
                # pool.start(worker)
            else:
                login_error_dialog = QErrorMessage(self)
                login_error_dialog.setWindowTitle("Error!")
                login_error_dialog.showMessage('Error: No Server Choice -> Please check Setup file name!')

            while not self.moveon:
                # lets only progress to the next job once the current job has finished submitting
                print(f"========= Waiting for job: {job} to submit =========")
                print(f"worker.threadfinished: {worker.threadfinished}")
                if worker.threadfinished:
                    self.itemindex = self.itemindex + 1
                    self.moveon = 1
                time.sleep(10)

        # Once the jobs have been submitted, need to remove from list so they dont get
        # submitted again -> Move to "submitted" list?
        for item in items:
            QListWidgetItem(item, self.SubmittedList)

        while self.listbox_view.count() > 0:
            self.listbox_view.takeItem(0)

        self.itemindex = 0
        # Job has started running so start updating the Job Table (if not already running)
        if self.TableUpdateStarted == 0:
            # we will 'click' the UpdateTableOnly button as this opens the shells to both clusters
            # so that they may be read
            self.UpdateTableOnlyButtonClick()

    def job_submitted_check(self, check):
        print({f"job_submitted_check: {check}"})
        # Job is finished so we need to increment the itemindex
        print(f"self.itemindex incremented from {self.itemindex} to {self.itemindex+1}")
        self.itemindex = self.itemindex + 1

    def ActionsArea(self):
        # This area defines the box where the action buttons are placed
        self.ActionsBox = QGroupBox('Actions')
        self.ActionsBox.setGeometry(QRect(20, 30, 211, 91))

        self.UpdateTableOnlyButton = QPushButton('Update Table only')
        self.OpenTableTableButton = QPushButton('Open Table Window')


        if self.w.isVisible():
            self.OpenTableTableButton.setDisabled(True)

        self.w.closingsignal.connect(self.setTableButtonDisabled)

        self.UpdateTableOnlyButton.clicked.connect(self.UpdateTableOnlyButtonClick)
        self.OpenTableTableButton.clicked.connect(self.OpenTableTableButtonClick)

        ActionsGrid = QGridLayout()

        ActionsGrid.addWidget(self.UpdateTableOnlyButton, 0, 0)
        ActionsGrid.addWidget(self.OpenTableTableButton, 0, 1)


        self.ActionsBox.setLayout(ActionsGrid)

    def OpenTableTableButtonClick(self):
        if not self.w.isVisible():
            self.w.show()

    def setTableButtonDisabled(self):
        self.OpenTableTableButton.setDisabled(False)


    def CancelJobArea(self):
        self.CancelBox = QGroupBox('Cancel Jobs')
        self.CancelBox.setGeometry(QRect(20, 30, 211, 91))

        self.CancelLXPLUSLabel = QLabel("Job to cancel (LXPLUS):")
        self.CancelSLURMLabel = QLabel("Job to cancel (SLURM):")
        self.CancelLXPLUSCB = QComboBox()
        self.CancelSLURMCB = QComboBox()
        self.CancelLXPLUSButton = QPushButton('Cancel Job (LXPLUS)')
        self.CancelSLURMButton = QPushButton('Cancel Job (SLURM)')

        self.CancelLXPLUSButton.clicked.connect(self.CancelLXPLUSButtonClick)
        self.CancelSLURMButton.clicked.connect(self.CancelSLURMButtonClick)

        # We will need to dynamically update the combo box with the current jobs

        CancelGrid = QGridLayout()
        CancelGrid.addWidget(self.CancelLXPLUSLabel, 0, 0)
        CancelGrid.addWidget(self.CancelLXPLUSCB, 0, 1)
        CancelGrid.addWidget(self.CancelLXPLUSButton, 0, 2)
        CancelGrid.addWidget(self.CancelSLURMLabel, 1, 0)
        CancelGrid.addWidget(self.CancelSLURMCB, 1, 1)
        CancelGrid.addWidget(self.CancelSLURMButton, 1, 2)

        self.CancelBox.setLayout(CancelGrid)

    def MakeScriptsOnlyButtonClick(self):

        # First check that the setup and CST files have been selected
        if self.SetupFileName == '':
           print(self.SetupFileName)
           print(self.CSTFileName)
           error_dialog = QErrorMessage(self)
           error_dialog.setWindowTitle("Error!")
           error_dialog.showMessage('Error: Setup file not selected!')
        else:
            print('Making Scripts')
            print(self.SetupFileName)
            print(self.SetupFileName.replace('/','\\'))

            if self.CSTCB.currentIndex() == 0:
                self.cstvers = 'cst2020'
            elif self.CSTCB.currentIndex() == 1:
                self.cstvers = 'cst2021'

            self.ServerChoice = self.ServerOptionCB.currentText()
            if self.ServerChoice == 'LXPLUS':
                self.ScriptsObj = Make_Scripts.scripts(self.SetupFileName.replace('/', '\\'))
                try:
                    self.ScriptsObj.loadyaml()
                except Exception as e:
                    print(f'Caught yaml error')
                    error_dialog = QErrorMessage(self)
                    error_dialog.setWindowTitle("Error!")
                    error_dialog.showMessage(f'Error: {e}')
                    return
                self.ScriptsObj.MakeSubFile(self.cstvers)
                self.ScriptsObj.MakeCSTScrpt(self.cstvers)
                self.Messages.append('Scripts have been created in ' + self.ScriptsObj.FileDirc)
            elif self.ServerChoice == 'SLURM':
                print("Made MPI SCript1")
                self.ScriptsObjSLURM = Make_MPI_Script.MPIScript(self.SetupFileName.replace('/', '\\'))
                print("Made MPI SCript2")
                self.ScriptsObjSLURM.loadyaml()

                self.ScriptsObjSLURM.MakeRunScript(self.cstvers)
                print("Made MPI SCripte")
                self.Messages.append('Scripts have been created in ' + self.ScriptsObjSLURM.FileDirc)


    def Error_Message(self, custommessage):
        print("begin Error_Message")
        print(f"message {custommessage}")

        button = QMessageBox.critical(
            self,
            "Error!!",
            custommessage,
            buttons=QMessageBox.StandardButton.Discard,
            defaultButton=QMessageBox.StandardButton.Discard,
        )
        print("end Error_Message")

        if button == QMessageBox.StandardButton.Discard:
            print("Discard!")
        else:
            print("Ignore!")



    def UpdateTableOnlyButtonClick(self):
        # This allows us to update the Job table without having to submit a job
        # We need to make connections to both the LXPLUS and SLURM clusters in order to
        # query them for the status of the running jobs

        #self.QueueGoButton.setDisabled(1)

        if self.loginCheck == 0:
            login_error_dialog = QErrorMessage(self)
            login_error_dialog.setWindowTitle("Error!")
            login_error_dialog.showMessage('Error: User has not logged in!')
            return

        self.host = "lxplus"
        print("Trying to connect")
        self.connection = OpenPutty.ssh(self.host, self.username, self.password, None)
        self.connection.open_shell()

        print("Connecting to server on ip: " + str(self.host) + ".")

        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")

        self.host = "hpc-batch.cern.ch"
        print("Trying to connect")
        self.connectionSLURM = OpenPutty.ssh(self.host, self.username, self.password, None)
        self.connectionSLURM.open_shell()

        print("Connecting to server on ip: " + str(self.host) + ".")

        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")

        # Now we run the UpdateJobTableTimer function on a different thread so it can
        # work in the background
        Tableworker = Worker(self.UpdateJobTableTimer)
        self.threadpool.start(Tableworker)



    def SubmitExecute(self, SetupFileName):
        print("SubmitExecute1")
        # This submits a Job to the LXPLUS cluster!!

        # This shouldnt be needed due to multithreading but I will keep it here just in case!
        #while self.CodeBusyCheck == 1:
        #    # wait until the code isnt busy doing something else (to stop crashes)
        #    print("WAITING TO EXECUTE CONDOR JOBS")
        #    time.sleep(10)



        self.CodeBusyCheck = 1
        self.UserEmail = self.Emailedit.text()
        # Here are the details that LXPLUX needs to sign in and connect
        self.host = "lxplus"
        port = 22
        self.initial = self.username[0]

        if self.CSTCB.currentIndex() == 0:
            self.cstvers = 'cst2020'
        elif self.CSTCB.currentIndex() == 1:
            self.cstvers = 'cst2021'

        # We create a script object to handle the creation of the LXPLUS scripts
        self.ScriptsObj = Make_Scripts.scripts(SetupFileName.replace('/', '\\'))
        #self.ScriptsObj.loadyaml()
        #if self.Try_Error(self.ScriptsObj.loadyaml, message='Could not read yaml file, please check!') == 0:
            # QErrorMessage(self).showMessage(f'Error: ');
        #    return
        #    pass
        try:
            print('trying to load yaml file!')
            self.ScriptsObj.loadyaml()
            print('trying to load yaml file2!')
        except:
            self.CodeBusyCheck = 0
            print('failed to load yaml file!')
            #self.Error_Message("Error!")
            raise


            #self.Error_Message()

        print("carrying on....")
        self.ScriptsObj.MakeSubFile(self.cstvers)
        self.ScriptsObj.MakeCSTScrpt(self.cstvers)
        print(self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][:-4] + '_Log.txt')

        print('==========================')
        print('Setup details: ')
        print('CPUs requested: ' + str(self.ScriptsObj.cfg['NProc']))
        print('==========================')

        self.Messages.append('==========================')
        self.Messages.append('Setup details: ')
        self.Messages.append('CPUs requested: ' + str(self.ScriptsObj.cfg['NProc']))
        self.Messages.append('==========================')
        self.connection = OpenPutty.ssh(self.host, self.username, self.password, self.ScriptsObj)

        print("Connecting to server on ip: " + str(self.host) + ".")
        print("Waiting to be submitted...")
        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")
        self.Messages.append("Waiting to be submitted...")


        # This function from OpenPutty.py sends the files over to the LXPLUS local directory and then submits them
        self.connection.execute_scripts()

        print('==========================')
        self.Messages.append('==========================')
        time.sleep(2)
        # Wait until we get a response from the LXPLUS cluster that the job has been submitted
        submitCheckCounter = 0
        while "submitted to cluster" not in self.connection.strdata.split("\\n")[-2]:
            # Sometimes, the files wont move over fast enough to be submitted the first time, so after 3 tries (30 secs)
            # Lets move over the files again and resubmit!
            if submitCheckCounter == 3:
                print(self.connection.strdata.split("\\n")[-5:-1])
                if not self.connection.CheckforFilesLXPLUS():
                    # if not all files are there, resend them
                    self.connection.execute_scripts()
                submitCheckCounter = -1 # to offset the increment in the while loop
                time.sleep(10)

            #if submitCheckCounter % 10 == 0:

            submitCheckCounter = submitCheckCounter + 1
            time.sleep(10)


        SubmitTime = datetime.now()
        current_time = SubmitTime.strftime("%H:%M:%S")
        print("Job submitted at " + current_time)
        self.Messages.append("Job submitted at " + current_time)

        #retrive the JOB ID of the submitted job
        self.ClusterID = self.connection.strdata.split("\\n")[-2].split()[-1].split(".")[0]

        # Write the Job details to the History file
        self.Historytableread.append([self.ClusterID, 'LXPLUS', self.ScriptsObj.cfg['CST File'][:-4], current_time])
        self.JobInfoHistoryFile = open(self.JobInfoFileDir, 'w')
        self.JobInfoHistoryFile.write(tabulate(self.Historytableread, headers="firstrow",floatfmt="10.0f"))
        self.JobInfoHistoryFile.close()

        self.host = "hpc-batch.cern.ch"
        print("Trying to connect")
        # Now we open a connection to the SLURM Cluster ready for the jobs to be queried later
        self.connectionSLURM = OpenPutty.ssh(self.host, self.username, self.password, None)
        self.connectionSLURM.open_shell()

        print("Connecting to server on ip: " + str(self.host) + ".")

        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")



        print("ClusterID: ", self.ClusterID)
        self.Messages.append('ClusterID: ' + self.ClusterID)

        print('Waiting for Job to start running...')
        self.Messages.append('Waiting for Job to start running...')

        # Once the Job is finished, need to reset everything so another Job can be submitted.
        self.JobFinishedCheck = 0
        self.JobExecutingCheck = 0
        self.Messages.append(
            'Writing log file to: ' + self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][
                                                                        :-4] + '_Log.txt')
        self.CodeBusyCheck = 0
        # Job has started running so start updating the Job Table (if not already running)
        #if self.TableUpdateStarted == 0:
        #    # we will 'click' the UpdateTableOnly button as this opens the shells to both clusters
        #    # so that they may be read
        #    self.UpdateTableOnlyButtonClick()

    def SubmitExecuteSLURM(self, SetupFileName):
        # This submits a Job to the SLURM cluster!!
        # This is very similar to the SubmitExecute() function for the LXPLUS cluster but with minor differences
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO EXECUTE SLURM JOBS")
            time.sleep(10)

        self.CodeBusyCheck = 1

        self.UserEmail = self.Emailedit.text()
        # Here are the details that SLURM needs to sign in and connect
        self.host = "hpc-batch.cern.ch"
        self.port = 22
        self.initial = self.username[0]

        if self.CSTCB.currentIndex() == 0:
            self.cstvers = 'cst2020'
        elif self.CSTCB.currentIndex() == 1:
            self.cstvers = 'cst2021'

        self.ScriptsObjSLURM = Make_MPI_Script.MPIScript(SetupFileName.replace('/', '\\'))
        #self.ScriptsObjSLURM.loadyaml()
        #if self.Try_Error(self.ScriptsObjSLURM.loadyaml, message='Could not read yaml file, please check!') == 0: return

        try:
            print('trying to load yaml file!')
            self.ScriptsObjSLURM.loadyaml()
            print('trying to load yaml file2!')
        except:
            self.CodeBusyCheck = 0
            print('failed to load yaml file!')
            #self.Error_Message("Error!")
            raise


            #self.Error_Message()

        print("carrying on....")


        self.ScriptsObjSLURM.MakeRunScript(self.cstvers)
        # Need to give the CERNBOX the time to update the created file so it can be moved to /hpcscratch/...
        time.sleep(10)


        print('==========================')
        print('Setup details: ')
        print('Nodes requested: ' + str(self.ScriptsObjSLURM.cfg['Nnode']))
        print('==========================')

        self.Messages.append('==========================')
        self.Messages.append('Setup details: ')
        self.Messages.append('Nodes requested: ' + str(self.ScriptsObjSLURM.cfg['Nnode']))
        self.Messages.append('==========================')

        time.sleep(1)

        print("Connecting to server on ip: " + str(self.host) + ".")
        print("Waiting to be submitted...")
        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")
        self.Messages.append("Waiting to be submitted...")

        self.connectionSLURM = OpenPutty.ssh(self.host, self.username, self.password, self.ScriptsObjSLURM)
        self.connectionSLURM.open_shell()
        # SLURM requires a password when you first open the connection =
        self.connectionSLURM.send_shell(self.password)
        self.connectionSLURM.execute_scripts_SLURM()

        time.sleep(5)
        #for h in self.connectionSLURM.strdata.split("\\n"):
            #print(h)

        print('==========================')
        self.Messages.append('==========================')
        time.sleep(2)

        SubmitTime = datetime.now()
        current_time = SubmitTime.strftime("%H:%M:%S")
        print("Job submitted at " + current_time)
        self.Messages.append("Job submitted at " + current_time)

        # Look at the last 10 lines of the SLURM output to find the submitted JOB ID

        SLURMJobRecieved = 0
        SLURMJobSubmitAttempts = 0
        while SLURMJobRecieved == 0:
            linecount = 0
            print("searching...")
            for i in self.connectionSLURM.strdata.split("\\n")[-10:]:
                print("i = ", linecount, i)
                if "Submitted batch job" in i:
                    #print(i)
                    #print(i.split())
                    #print(i.split()[-1].replace('\\r',''))
                    self.ClusterID = i.split()[-1].replace('\\r','')
                    SLURMJobRecieved = 1
                linecount = linecount + 1
            time.sleep(1)
            SLURMJobSubmitAttempts = SLURMJobSubmitAttempts + 1
            # If 10 attempts have been made to Submit the CST and setup files, then transfer the files to hpc-scratch
            # and try again
            if SLURMJobSubmitAttempts == 10:
                self.connectionSLURM.execute_scripts_SLURM()
                SLURMJobSubmitAttempts = 0

        # Write the Job details to the History file
        self.Historytableread.append([self.ClusterID, 'SLURM', self.ScriptsObjSLURM.cfg['Run File'], current_time])
        self.JobInfoHistoryFile = open(self.JobInfoFileDir, 'w')
        self.JobInfoHistoryFile.write(tabulate(self.Historytableread, headers="firstrow",floatfmt="10.0f"))
        self.JobInfoHistoryFile.close()

        self.host = "hpc-batch.cern.ch"

        # Job has started running so start updating the Job Table
        print("self.TableUpdateStarted: ", self.TableUpdateStarted)


        print("ClusterID: ", self.ClusterID)
        self.Messages.append('ClusterID: ' + self.ClusterID)

        print('Waiting for Job to start running...')
        self.Messages.append('Waiting for Job to start running...')

        # Once the Job is finished, need to reset everything so another Job can be submitted.
        self.JobFinishedCheck = 0
        self.JobExecutingCheck = 0
        self.CodeBusyCheck = 0
        if self.TableUpdateStarted == 0:
            #we will 'click' the UpdateTableOnly button as this opens the shells to both clusters
            # so that they may be read
            self.UpdateTableOnlyButtonClick()
        #self.Messages.append(
        #    'Writing log file to: ' + self.ScriptsObj.FileDirc + '\\' + self.ScriptsObjSLURM.cfg['Run File'][
        #                                                                :-4] + '_Log.txt')


    def SubmitButtonClick(self):
        # When Submit Job is clicked, first need to check that the Setup and CST files are selected
        # If not --> throw error!
        self.UserEmail = self.Emailedit.text()
        if self.SetupFileName == '':
           print(self.SetupFileName)
           error_dialog = QErrorMessage(self)
           error_dialog.setWindowTitle("Error!")
           error_dialog.showMessage('Error: Setup file not selected!')
        #elif self.CSTFileName == '':
        #    error_dialog = QErrorMessage(self)
        #    error_dialog.setWindowTitle("Error!")
        #    error_dialog.showMessage('Error: CST file not selected!')
        elif self.loginCheck == 0:
            error_dialog = QErrorMessage(self)
            error_dialog.setWindowTitle("Error!")
            error_dialog.showMessage('Error: Login details not entered!')
        else:
            while self.CodeBusyCheck == 1:
                # wait until the code isnt busy doing something else (to stop crashes)
                print("WAITING TO SUBMIT")
                time.sleep(10)

            self.ServerChoice = self.ServerOptionCB.currentText()

            self.Messages.append('Good to go!')
            print(self.ServerChoice)

            # Need to check which server has been selected and execute the appropriate function
            if self.ServerChoice == 'LXPLUS':
                self.Messages.append('Submitting job to LXPLUS Cluster')
                worker = Worker(self.SubmitExecute)
                self.threadpoolsubmit = QThreadPool()
                self.threadpoolsubmit.start(worker)
            elif self.ServerChoice == 'SLURM':
                self.Messages.append('Submitting job to SLURM Cluster')
                workerSLURM = Worker(self.SubmitExecuteSLURM)
                self.threadpoolsubmitSLURM = QThreadPool()
                self.threadpoolsubmitSLURM.start(workerSLURM)

    def OptionsArea(self):
        self.OptionsBox = QGroupBox('Options')
        self.OptionsBox.setGeometry(QRect(20, 30, 211, 91))

        self.CSTCBLabel = QLabel("CST Version (single job submission only):")
        self.CSTCB = QComboBox()
        self.CSTCB.addItem("CST2020")
        self.CSTCB.addItem("CST2021")

        print(self.CSTCB.currentIndex())
        self.CSTCB.currentIndexChanged.connect(self.CSTSelection)

        self.ServerOptionLabel = QLabel("Use Server (single job submission only):")
        self.ServerOptionCB = QComboBox()
        self.ServerOptionCB.addItem("LXPLUS")
        self.ServerOptionCB.addItem("SLURM")

        self.SLURMOutputOptionLabel = QLabel("Output SLURM Messages? (experimental!)")
        self.SLURMOutputOption = QCheckBox()

        self.SLURMReturnFilesOptionLabel = QLabel("Return LXPLUS error/log/output files?")
        self.SLURMReturnFilesOption = QCheckBox()

        OptionsGrid = QGridLayout()
        OptionsGrid.addWidget(self.CSTCBLabel,0,0)
        OptionsGrid.addWidget(self.CSTCB,0,1)
        OptionsGrid.addWidget(self.ServerOptionLabel, 2, 0)
        OptionsGrid.addWidget(self.ServerOptionCB, 2, 1)
        OptionsGrid.addWidget(self.SLURMOutputOptionLabel, 3, 0)
        OptionsGrid.addWidget(self.SLURMOutputOption, 3, 1)
        OptionsGrid.addWidget(self.SLURMReturnFilesOptionLabel, 4, 0)
        OptionsGrid.addWidget(self.SLURMReturnFilesOption, 4, 1)


        self.OptionsBox.setLayout(OptionsGrid)

    def CSTSelection(self):
        print(self.CSTCB.currentIndex())

    def CancelLXPLUSButtonClick(self):
        # This function allows the user to cancel a submitted job on the LXPLUS cluster
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO CANCEL CONDOR JOBS")
            # This will ONLY appear in the console
            time.sleep(1)

        self.CodeBusyCheck = 1
        self.command3 = "condor_rm " + self.CancelLXPLUSCB.currentText()
        self.connection.send_shell("\x03")
        time.sleep(1)
        self.connection.send_shell(self.command3)
        print("command " + self.command3 + " has been sent.")
        while "removal" not in self.connection.strdata.split("\\n")[-2]:
            # wait until the Job has been marked for removal in the LXPLUS cluster
            pass
        print("Job with ClusterID " + self.CancelLXPLUSCB.currentText() + " has been marked for removal.")
        self.Messages.append("Job with ClusterID " + self.CancelLXPLUSCB.currentText() + " has been marked for removal.")
        # We need to keep a record of the jobs that the user has manually cancelled so we can display this on the Job
        # Table. The LXPLUS cluster does not keep a record of this!
        self.CancelledJobs.append(self.CancelLXPLUSCB.currentText())
        self.CodeBusyCheck = 0

    def CancelSLURMButtonClick(self):
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO CANCEL SLURM JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1
        self.command4 = "scancel " + self.CancelSLURMCB.currentText()
        self.connectionSLURM.send_shell("\x03")
        time.sleep(1)
        self.connectionSLURM.send_shell(self.command4)
        print("command " + self.command4 + " has been sent.")
        #while "removal" not in self.connectionSLURM.strdata.split("\\n")[-2]:
        #    pass
        print("Job with ClusterID " + self.CancelSLURMCB.currentText() + " has been marked for removal.")
        self.Messages.append("Job with ClusterID " + self.CancelSLURMCB.currentText() + " has been marked for removal.")
        # We need to keep a record of the jobs that the user has manually cancelled so we can display this on the Job
        # Table. The SLURM cluster does not keep a record of this!
        self.CancelledJobs.append(self.CancelSLURMCB.currentText())
        self.CodeBusyCheck = 0

    def JobTableArea(self):
        # This function defines the area where the Job Table is displayed in the GUI
        self.JobTableBox = QGroupBox("Job Status")
        #self.JobTableBox.setGeometry(QtCore.QRect(20, 300, 511, 91))
        self.JobTableBox.setMinimumSize(600,400)
        self.JobTableBox.setGeometry(0, 0, 4000, 2000)
        self.JobStatusLabel0 = QLabel()
        self.JobStatusLabel0.setText("Status: ")
        self.JobStatusLabel = QLabel()
        self.JobStatusLabel.setText("Idle")
        self.JobTable = QTableWidget()
        #self.JobTable.setGeometry(QtCore.QRect(10, 90, 331, 111))
        self.JobTable.setGeometry(0, 0, 4000, 2000)

        self.JobTable.setRowCount(20)
        self.JobTable.setColumnCount(7)

        self.JobTable.setHorizontalHeaderLabels(['Job ID','Submission', 'State', 'Server', 'Partition', 'Nodes', 'Name'])
        self.JobTable.horizontalHeader().setDefaultSectionSize(100)

        #self.ShowOutputButton = QPushButton("Show Output (SLURM only!)")
        #self.ShowOutputButton.clicked.connect(self.show_new_window)

        #self.ShowOutputButton = QCheckBox("Show Output (SLURM only!)")
        #self.ShowOutputButton.setChecked(True)

        # Initially we will disable the Output button
        # We will enable it when the Table is filled
        #self.ShowOutputButton.setEnabled(False)

        TableGrid= QGridLayout()
        TableGrid.addWidget(self.JobStatusLabel, 0, 1)
        TableGrid.addWidget(self.JobStatusLabel0, 0, 0)
        TableGrid.addWidget(self.JobTable,1,0,20,10)
        #TableGrid.addWidget(self.ShowOutputButton, 1, 0)
        self.JobTableBox.setLayout(TableGrid)

    def UpdateSLURMJobOutputTabs(self):
        self.TabJobIDs = []
        for i in self.UniqueJobTrackListSLURM:
            self.TabJobIDs.append(i[0])
        # We need to keep the Tab and Messages objs in a list so we can access them easily to edit them
        #self.TabList = []
        #self.TabMessagesList = []

        print("self.TabJobIDs:", self.TabJobIDs)
        #for k in self.TabJobIDs:
        #    self.AddJobTabSLURM(k)
        #self.AddJobTabSLURM('11238')

        print("printing slurm output1")
        NumTabsIndex = 0
        for i in self.TabJobIDs:
            # to stop from printing the file contents one after the other
            # clear the text in the message box and then reprint the file contents
            print("printing slurm output1.1")
            print("NumTabsIndex:", NumTabsIndex)
            # self.TabMessagesList[NumTabsIndex].clearHistory()
            print("printing slurm output2")
            print("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/slurm-" + i + ".out")
            # If the CST Sim has not yet started running then the output file will not exist and the code will crash
            # i.e. we need to check that the output file exists first!
            if os.path.isfile("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/slurm-" + i + ".out"):
                with open("H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/slurm-" + i + ".out", 'r') as outfile:
                    print("printing slurm output3")
                    # self.TabMessagesList[NumTabsIndex].append("=====================")
                    print("printing slurm output3.1")
                    temptext = "SLURM output information for Job " + i + "\n ============================================= \n " + outfile.read()
                    print(temptext[-50:])
                    print("printing slurm output3.2")
                    # self.TabMessagesList[NumTabsIndex].clear()
                    print("printing slurm output3.3")
                    time.sleep(2)
                    print("printing slurm output3.4")
                    # self.TabMessagesList[NumTabsIndex].clear()
                    print("printing slurm output3.5")
                    time.sleep(2)
                    print("printing slurm output3.6")
                    self.TabMessagesList[NumTabsIndex].clear()
                    time.sleep(2)
                    print("printing slurm output3.7")
                    self.TabMessagesList[NumTabsIndex].setPlainText(temptext)
                    print("printing slurm output4")

                    NumTabsIndex = NumTabsIndex + 1
                    print("printing slurm output6")
            else:
                temptext = "This Job (" + i + ") has not started yet."
                self.TabMessagesList[NumTabsIndex].setPlainText(temptext)
                NumTabsIndex = NumTabsIndex + 1


    def AddJobTabSLURM(self, JobID):
        print("AddJobTab1")
        self.TabList.append(QWidget())
        print("AddJobTab1.1")
        tab = self.TabList[self.NumTabsIndex]
        print("AddJobTab2")

        tablayout = QVBoxLayout()
        print("AddJobTab2.1")
        #self.SLURMJobOutputTabs.addTab(tab, JobID)
        print("AddJobTab3")

        #self.TabMessagesList.append(QTextBrowser())
        self.TabMessagesList.append(QPlainTextEdit())
        TabMessages = self.TabMessagesList[self.NumTabsIndex]
        tablayout.addWidget(TabMessages)
        tab.setLayout(tablayout)
        print("AddJobTab4")

        #TabMessages.append("This is the output for Job: " + JobID)
        print("AddJobTab5")
        self.SLURMJobOutputTabs.addTab(tab, JobID)
        self.NumTabsIndex = self.NumTabsIndex + 1

    def UpdateJobTableCondor(self):
        #while self.CodeBusyCheck == 1:
        #    # wait until the code isnt busy doing something else (to stop crashes)
        #    print("WAITING TO UPDATE CONDOR JOBS")
        #    time.sleep(20)

        self.CodeBusyCheck = 1
        print("UpdateJobTable() starting")
        self.allJobsListCondor = []
        print("here0")

        # send command to the LXPLUS Cluster in order to read the Job output
        self.connection.send_shell('condor_q -nobatch')

        time.sleep(5)

        # need to find the latest update in the output
        updateposlist = []
        updateposindex = 0
        updateposchecks = 0
        while updateposlist == []:
            for i in self.connection.strdata.split("\\n"):
                #print("updateposindex: ", updateposindex, ", i: ", i)
                if "OWNER" in i:
                    updateposlist.append(updateposindex)
                updateposindex = updateposindex + 1
            print("updateposlist: ", updateposlist)
            # need to reset index if no update has been found!
            if updateposlist == []:
                updateposindex = 0
                # lets keep track of how many times we have checked the output
                updateposchecks = updateposchecks + 1
            time.sleep(2)
            if updateposchecks == 20:
                # We have checked the output and cannot find the job list, print message and see whats happening
                print("updateposchecks = 20 and cant find the Jobs!")
                print("LXPLUS output: ", self.connection.strdata.split("\\n"))
                # If we cant find the Jobs after 20 loops then we should allow the code to continue to function
                # It is possible that this could be caused by the connection to the LXPLUS cluster which may be out
                # of our control! -> Maybe add checkbox to give option of skipping checking LXPLUS/SLURM Jobs?
                self.CodeBusyCheck = 0
                self.Messages.append("Could not find any LXPLUS Jobs! -> Skipping")
                return
        latestupdatepos = updateposlist[-1] + 1
        #print("latestupdatepos: ", latestupdatepos)

        #print(self.connection.strdata.split("\\n"))
        #print(self.connection.strdata.split("\\n")[latestupdatepos])
        # We have to read the output of the LXPLUS cluster to dynamically find the number of active jobs on the server
        #if self.connection.strdata.split("\\n")[latestupdatepos] == '\\r':
        print(self.connection.strdata.split("\\n")[latestupdatepos])
        if self.username not in self.connection.strdata.split("\\n")[latestupdatepos]:
            self.TotalJobNum = 0
            self.CodeBusyCheck = 0
            return
            # If there are no jobs found, then exit out of the UpdateJobTableCondor function
        else:
            totaljobsearch = 0
            while self.username in self.connection.strdata.split("\\n")[latestupdatepos + totaljobsearch]:
                # the users username should appear in the line if there is a Job on the cluster, otherwise theres no job
                print("totaljobsearch: ", totaljobsearch)
                #print(self.connection.strdata.split("\\n")[latestupdatepos + totaljobsearch])
                # if the next line is not '\\r' then there must be a job
                # increment through lines until '\\r' is found, counting the jobs as we go
                totaljobsearch = totaljobsearch + 1

            #Once the while loop is stopped, we have reached the end of the list of jobs so assign the TotalJobNum
            self.TotalJobNum = totaljobsearch

        print(self.TotalJobNum)
        print("here1")

        # For each job we want self.allJobsListCondor[j]=["JOB ID", "NAME", "SUBMISSION DATE/TIME", "RUNTIME", "STATUS", "SERVER"]
        for j in range(self.TotalJobNum):
            self.allJobsListCondor.append([])
            print(f"j: {j}")
            print(f"latestupdatepos: {latestupdatepos}")
            print("len(self.connection.strdata.split(\"\\n\")):", self.connection.strdata.split("\\n"))
            #Job_ID
            line= self.connection.strdata.split("\\n")[latestupdatepos + j].split()
            print(f"Job Info: {line}")


            print(self.connection.strdata.split("\\n")[latestupdatepos + j +1].split())
            self.allJobsListCondor[j].append(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[0].split('\\r')[0])

            # NAME
            print(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[8])
            self.allJobsListCondor[j].append(
                self.connection.strdata.split("\\n")[latestupdatepos + j].split()[8].split('\\r')[0])
            print("here1.1")
            # Submit date/time
            print(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[2] + " - " + self.connection.strdata.split("\\n")[latestupdatepos + j].split()[3])
            self.allJobsListCondor[j].append(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[2] + " - " + self.connection.strdata.split("\\n")[latestupdatepos + j].split()[3])
            print("here1.2")
            # RUNTIME
            print(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[4])
            self.allJobsListCondor[j].append(
                self.connection.strdata.split("\\n")[latestupdatepos + j].split()[4])

            print("here2")
            # Here we look at the output and work out what the current state of the job is (Idle/Running, etc)
            if self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5] == 'I':
                # Job is IDLE
                self.allJobsListCondor[j].append('Idle')
            elif self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5] == 'R':
                # Job is RUNNING
                self.allJobsListCondor[j].append('Running')
            elif self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5] == 'H':
                # Job is HOLD
                self.allJobsListCondor[j].append('Hold')
            elif self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5] == 'C':
                # Job is COMPLETED
                self.allJobsListCondor[j].append('Completed')
            elif self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5] == 'X':
                # Job is Removed
                self.allJobsListCondor[j].append('Removed')
            elif self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5] == 'S':
                # Job is SUSPENDED
                self.allJobsListCondor[j].append('Suspended')
            elif '<' in self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5]:
                # Job is TRANSFERRING INPUT
                self.allJobsListCondor[j].append('Transferring Input')
            elif '>' in self.connection.strdata.split("\\n")[latestupdatepos + j].split()[5]:
                # Job is TRANSFERRING OUTPUT
                self.allJobsListCondor[j].append('Transferring Output')
            else:
                print("here.2.01")
                print("self.UniqueJobTrackListCondor: ", self.UniqueJobTrackListCondor)
                print("self.allJobsListCondor[j]: ", self.allJobsListCondor[j])
                print("self.UniqueJobTrackListCondor[j][0]: ", self.UniqueJobTrackListCondor[j][0])
                print("self.allJobsListCondor[j][0]: ", self.allJobsListCondor[j][0])
                # Sometimes if we catch the output just as the Job finishes, it will show as "finished" in the output
                # Otherwise the Job just completely disappears in the output
                for i in range(len(self.UniqueJobTrackListCondor)):
                    if self.UniqueJobTrackListCondor[i][0] != self.allJobsListCondor[j][0]:
                        print("Job " + self.allJobsListCondor[j][0] + " has finished!")
                        self.allJobsListCondor[j].append('Finished')
            print("here2.1")
            # Need to add to each Job that it belongs to the LXPLUS Cluster
            self.allJobsListCondor[j].append('LXPLUS')
            UniqueJobIDList = []
            for i in self.UniqueJobTrackListCondor:
                UniqueJobIDList.append(i[0])
            print("here2.2")

            if self.allJobsListCondor[j] not in self.UniqueJobTrackListCondor:
                if self.allJobsListCondor[j][0] in UniqueJobIDList:
                    print("here2.3")
                    # if the Job info is not unique but the Job ID is already stored, then the STATE of the job has updated (IDLE -> RUNNING)
                    # therefore we must update the entry in the UniqueJobTrackListCondor
                    print("self.UniqueJobTrackListCondor: ", self.UniqueJobTrackListCondor)
                    print("self.allJobsListCondor[j]: ", self.allJobsListCondor[j])
                    for i in self.UniqueJobTrackListCondor:
                        if i[0] in self.CancelledJobs:
                            # check if job has been cancelled by the user
                            i[4] = 'Cancelled'
                        elif i[0] == self.allJobsListCondor[j][0]:
                            i[4] = self.allJobsListCondor[j][4]
                            # We must also update the current RUNTIME
                            print("Updating RUNTIME for Job ", i[0])
                            i[3] = self.allJobsListCondor[j][3]

                    print("here2.31")
                    # New job has started so print message of the time
                    ExecuteTime = datetime.now()
                    current_time = ExecuteTime.strftime("%H:%M:%S")
                    #print('Job: ' + self.allJobsListCondor[j][0] + ' started running at ' + current_time)
                    #self.Messages.append('Job: ' + self.allJobsListCondor[j][0] + ' started running at ' + current_time)
                    print("here2.4")
                else:
                    print("here2.5")
                    # if the Job info isnt unique and the JobID isnt stored, then append to the Unique list
                    # so we can keep a track of all the Jobs that are running and those that disappear!
                    self.UniqueJobTrackListCondor.append(self.allJobsListCondor[j])
                    # The job has been added to the Unique list, so keep check if a Finished Email has been sent yet
                    self.EmailBeenSentCheckCondor.append([])
                    self.EmailBeenSentCheckCondor[j].append(self.UniqueJobTrackListCondor[j][0])
                    self.EmailBeenSentCheckCondor[j].append(0)
                    # We also need to add the job to the Cancel Combo Box so it can be canceled later
                    self.CancelLXPLUSCB.addItem(self.UniqueJobTrackListCondor[j][0])
                    print("here2.51")

        #print("self.UniqueJobTrackListCondor[j][0]: ", self.UniqueJobTrackListCondor[j][0])
        print("self.CancelledJobs: ", self.CancelledJobs)
        for k in self.UniqueJobTrackListCondor:
            if k[0] in self.CancelledJobs:
                # check if the job has been cancelled by user
                k[2] = 'Cancelled'
                print("JOB ", k[0], " HAS BEEN CANCELLED")
        print("UniqueJobTrackListSLURM: ", self.UniqueJobTrackListCondor)

        print("here3")

        currentJobIDList = []
        for i in self.allJobsListCondor:
            currentJobIDList.append(i[0])
        print("Current Job list: ", currentJobIDList)

        print("here5")

        # Now we have to check if the stored unique Job Ids is in the SSH output -> if not, the job has finished
        for i in self.UniqueJobTrackListCondor:
            if i[0] not in currentJobIDList:
                # job ID has disappeared -> set to finished
                if i[4] == 'Finished':
                    pass
                else:
                    i[4] = 'Finished'
                    # Check if 'Finished' message has been printed already
                    finishTime = datetime.now().strftime("%H:%M:%S")
                    #print('Job: ' + i[0] + ' finished at ' + finishTime)
                    #self.Messages.append('Job: ' + i[0] + ' finished at ' + finishTime)
                    # The job is finished but we need to see if it was aborted or not!
                    print('i[0]: ',i[0] )
                    self.connection.send_shell('condor_wait -status log/cst.' + i[0].split('.')[0] + '.log')
                    time.sleep(1)
                    for k in self.connection.strdata.split("\\n")[latestupdatepos::-1]:
                        if i[0] in k:
                            if 'aborted' in k:
                                # Job has been aborted!
                                i[2] = 'Aborted'
                                aborttime = datetime.now().strftime("%H:%M:%S")
                                print('Job: ' + i[0] + ' was aborted by the LXPLUS Cluster at ' + aborttime)
                                self.Messages.append('Job: ' + i[0] + ' was aborted by the LXPLUS Cluster at ' + aborttime)

        # Now output the updated Unique Job info into the GUI Table
        print("here7")

        # Update the Logfile at the end of each Table Update
        #if self.ScriptsObj != None:
        #    LogFile = open(self.ScriptsObj.FileDirc + self.ScriptsObj.cfg['CST File'][:-4] + '_Log.txt', "w")
        #    LogFile.write(self.Messages.toPlainText())
        #    LogFile.close()
        print("here8")

        # Sometimes when jobs 'finish' they can be aborted by the LXPLUS cluster
        # so it can be useful to see the error/log/output files on the AFS directory
        # Once job has finished, transfer these files to the users local PC
        print("self.SLURMReturnFilesOption.isChecked(): ", self.SLURMReturnFilesOption.isChecked())
        if self.ScriptsObj != None and self.SLURMReturnFilesOption.isChecked():
            for i in self.UniqueJobTrackListCondor:
                if i[2] == 'Finished':
                    EOSOutPutDir = self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/' + 'LXPLUS_output/'
                    LocalOutPutDir = self.ScriptsObj.cfg['EOS Res'].replace('/eos/user/'+self.username[0]+'/'+self.username, 'C:/Users/'+self.username+'/cernbox') + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/' + 'LXPLUS_output/'
                    print('EOSOutPutDir: ', EOSOutPutDir)
                    print('LocalOutPutDir: ', LocalOutPutDir)
                    if not os.path.exists(LocalOutPutDir):
                        # if weve already moved the files, we dont want to do it again
                        print('Moving LXPLUS output files...')
                        # os.mkdir(EOSOutPutDir)
                        print('Creating Directory...')
                        # Move .tar file into a folder which specifies the time and ID of the Job for easier reading
                        self.connection.send_shell("mkdir " + self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/' + '\n' +
                                                   "cp " + self.ScriptsObj.cfg['EOS Res'] + self.ScriptsObj.cfg['CST File'][:-4] + '.tar ' + self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/'  "\n")
                        print("Here9")

                        MoveFinishedFilesCommand = "mkdir " + EOSOutPutDir + "\n" + \
                                                "cd " + self.ScriptsObj.cfg['AFS Dir'] + "error \n " + \
                                                "cp cst." + i[0] + ".err " + EOSOutPutDir + "\n" + \
                                               "cp cst." + i[0].split('.')[0] + ".err " + EOSOutPutDir + "\n" + \
                                               "cd " + self.ScriptsObj.cfg['AFS Dir'] + "log \n " + \
                                               "cp cst." + i[0] + ".log " + EOSOutPutDir + "\n" + \
                                               "cp cst." + i[0].split('.')[0] + ".log " + EOSOutPutDir + "\n" + \
                                               "cd " + self.ScriptsObj.cfg['AFS Dir'] + "output \n " + \
                                               "cp cst." + i[0] + ".out " + EOSOutPutDir + "\n" + \
                                               "cp cst." + i[0].split('.')[0] + ".out " + EOSOutPutDir + "\n"
                        #print(MoveFinishedFilesCommand)

                        self.connection.send_shell(MoveFinishedFilesCommand)
                        self.Messages.append('Moved results and LXPlus log/error/out files to: ' + self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/')

        self.CodeBusyCheck = 0

    def UpdateJobTableSLURM(self):
        # This function is very similar to the UpdateJobTableCondor function above with some minor differences
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO UPDATE SLURM JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1
        print("UpdateJobTable() SLURM starting")
        self.allJobsListSLURM = []
        print("here0")
        # Open new SSH terminal for q checks?
        # self.connection2 = OpenPutty.ssh(self.host, self.username, self.password, self.ScriptsObj)
        self.connectionSLURM.send_shell('squeue -u ' + self.username + " -o \"%.18i %.9P %.100j %.8u %.2t %.10M %.6D %R %V\"")
        # These extra options "-o" allow us to choose how to format the output from squeue -> We can now see the full
        # name of the jobs on the SLURM Cluster!

        #print('squeue -u ' + self.username)
        time.sleep(5)

        # need to find the latest update in the output
        updateposlist = []
        updateposindex = 0
        while updateposlist == []:
            for i in self.connectionSLURM.strdata.split("\\n"):
                # print("updateposindex: ", updateposindex, ", i: ", i)
                if "JOBID" in i:
                    updateposlist.append(updateposindex)
                updateposindex = updateposindex + 1
            print("updateposlist: ", updateposlist)
            # need to reset index if no update has been found!
            if updateposlist == []:
                updateposindex = 0
                print(self.connectionSLURM.strdata.split("\\n")[-15:])
            time.sleep(2)
        latestupdatepos = updateposlist[-1] + 1
        print("latestupdatepos (SLURM): ", latestupdatepos)

        #print(self.connectionSLURM.strdata.split("\\n"))
        #print(self.connectionSLURM.strdata.split("\\n")[latestupdatepos])
        if self.connectionSLURM.strdata.split("\\n")[latestupdatepos] == '\\r' or self.connectionSLURM.strdata.split("\\n")[latestupdatepos] == '\\r\\r':
            self.TotalJobNum = 0
            # If there are no jobs found, then exit out of the UpdateJobTableSLURM function
            self.CodeBusyCheck = 0
            return
        else:
            totaljobsearch = 0
            #while self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r'\r' and \
            #        self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r"'b'\r" and \
            #        self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r"\r\r" and \
            #        self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r"'b'\r\r":
            while self.username in self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch]:
                # your username should appear in the line if you have a Job running, so check for this
                print("totaljobsearch: ", totaljobsearch)
                #print(self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch])
                # Read through lines until your username is no longer found
                totaljobsearch = totaljobsearch + 1

            # Once the while loop is stopped, we have reached the end of the list of jobs so assign the TotalJobNum
            self.TotalJobNum = totaljobsearch

        print(self.TotalJobNum)
        print("here1")

        # For each job we want ["JOB ID", "NAME", "SUBMISSION DATE/TIME", "RUNTIME", "STATUS", "SERVER", "PARTITION", "NODES"]
        for j in range(self.TotalJobNum):
            self.allJobsListSLURM.append([])
            # sometimes there is a "'b'" at the start of the line so we need to check for this then fix it
            if self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split()[0] == "'b'":
                print("'b' detected!")
                #self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split().pop(0)
                SLURM_Line_Array = self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split()[1:]
            else:
                SLURM_Line_Array = self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split()
            # Job_ID
            self.allJobsListSLURM[j].append(SLURM_Line_Array[0])

            # Add the Name (shortened version) of the Job
            self.allJobsListSLURM[j].append(SLURM_Line_Array[2])

            # Submit date/time (time running)

            self.allJobsListSLURM[j].append(SLURM_Line_Array[8].replace('\\r','').replace('T', '--'))

            # Runtime
            self.allJobsListSLURM[j].append(SLURM_Line_Array[5] + ' (time running)' )

            print("here2")
            # STATUS
            # Here we work out what state each job is in by reading the SLURM output
            if SLURM_Line_Array[4] == 'PD':
                # Job is PENDING
                self.allJobsListSLURM[j].append('Pending')
            elif SLURM_Line_Array[4] == 'R':
                # Job is RUNNING
                self.allJobsListSLURM[j].append('Running')
            else:
                print("here.2.01")
                print("self.UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)
                print("self.allJobsListSLURM[j]: ", self.allJobsListSLURM[j])
                print("self.UniqueJobTrackListSLURM[i][0]: ", self.UniqueJobTrackListSLURM[j][0])
                print("self.allJobsListSLURM[j][0]: ", self.allJobsListSLURM[j][0])

                # We only want to compare against the Unique list AFTER the initial Table Update
                if self.First_Update_Check == 0:
                    for i in range(len(self.UniqueJobTrackListSLURM)):
                        if self.UniqueJobTrackListSLURM[i][0] != self.allJobsListSLURM[j][0]:
                            print("Job " + self.allJobsListSLURM[j][0] + " has finished!")
                            self.allJobsListSLURM[j].append('Finished')

            print("here2.1")
            self.allJobsListSLURM[j].append('SLURM')

            # Add the Partition the job is running on and the Nodes requested
            self.allJobsListSLURM[j].append(SLURM_Line_Array[1])
            self.allJobsListSLURM[j].append(SLURM_Line_Array[6])




            UniqueJobIDList = []
            for i in self.UniqueJobTrackListSLURM:
                UniqueJobIDList.append(i[0])
            print("here2.2")
            print("self.allJobsListSLURM: ", self.allJobsListSLURM)
            print("self.UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)
            print("self.CancelledJobs: ", self.CancelledJobs)

            if self.allJobsListSLURM[j] not in self.UniqueJobTrackListSLURM:
                print("here2.21")
                if self.allJobsListSLURM[j][0] in UniqueJobIDList:
                    print("here2.3")
                    # if the Job info is not unique but the Job ID is already stored, then the STATE or TIME of the job has updated (IDLE -> RUNNING)
                    # therefore we must update the entry in the UniqueJobTrackListSLURM
                    print("self.UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)
                    print("self.allJobsListSLURM[j]: ", self.allJobsListSLURM[j])
                    for i in self.UniqueJobTrackListSLURM:
                        if i[0] == self.allJobsListSLURM[j][0]:

                            if i[2] != self.allJobsListSLURM[j][2]:
                                #if the STATUS is different than that alread stored then update and print that
                                # job is now running and the current time
                                #update the STATE of the Job
                                i[2] = self.allJobsListSLURM[j][2]
                                ExecuteTime = datetime.now()
                                current_time = ExecuteTime.strftime("%H:%M:%S")
                                print('Job: ' + self.allJobsListSLURM[j][0] + ' started running at ' + current_time)
                                self.Messages.append(
                                    'Job: ' + self.allJobsListSLURM[j][0] + ' started running at ' + current_time)
                                print("here2.4")


                            #update the TIME RUNNING of the Job
                            #This should be done at every update!
                            i[1] = self.allJobsListSLURM[j][1]

                    print("here2.32")

                else:
                    print("here2.33")
                    # if the Job info isnt unique and the JobID isnt stored, then append to the Unique list
                    self.UniqueJobTrackListSLURM.append(self.allJobsListSLURM[j])
                    # The job has been added to the Unique list, so keep check if a Finished Email has been sent yet
                    self.EmailBeenSentCheckSLURM.append([])
                    self.EmailBeenSentCheckSLURM[j].append(self.UniqueJobTrackListSLURM[j][0])
                    self.EmailBeenSentCheckSLURM[j].append(0)
                    # We also need to add the job to the Cancel Combo Box so it can be canceled
                    self.CancelSLURMCB.addItem(self.UniqueJobTrackListSLURM[j][0])
                    print("here2.5")

        print("here3.0")
        #print("self.UniqueJobTrackListSLURM[j][0]: ", self.UniqueJobTrackListSLURM[j][0])
        print("self.CancelledJobs: ", self.CancelledJobs)
        for k in self.UniqueJobTrackListSLURM:
            if k[0] in self.CancelledJobs:
                # check if the job has been cancelled by user
                k[2] = 'Cancelled'
                print("JOB ", k[0], " HAS BEEN CANCELLED")
        print("UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)

        print("here3")

        currentJobIDList = []
        for i in self.allJobsListSLURM:
            currentJobIDList.append(i[0])
        print("Current Job list (SLURM): ", currentJobIDList)

        print("here4")

        # Now we have to check if the stored unique Job Ids is in the SSH output -> if not, the job has finished
        for i in self.UniqueJobTrackListSLURM:
            print("Checking to see if Job " + i[0] + " has finished:")
            print("currentJobIDList ", currentJobIDList)
            if i[0] not in currentJobIDList:
                # job ID has disappeared -> set to finished
                print("Job " + i[0] + " has finished!")
                i[4] = 'Finished'

        # Now output the updated Unique Job info into the GUI Table

        # Update the Logfile at the end of each Table Update
        if self.ScriptsObj != None:
            LogFile = open(self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][:-4] + '_Log.txt', "w")
            LogFile.write(self.Messages.toPlainText())
            LogFile.close()
        print("here8")

        # We will now copy over the SLURM output files to the local directory
        self.connectionSLURM.send_shell("cd /hpcscratch/user/" + self.connectionSLURM.username + "/")
        for k in self.UniqueJobTrackListSLURM:
            self.connectionSLURM.send_shell("cp slurm-" + k[0] + ".out /eos/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/")

        print("SLURM output files have been moved to: H:/user/" + self.username[0] + "/" + self.username + "/Documents/Slurm_Output_Files/")
        self.CodeBusyCheck = 0

    def PrintJobTable(self):
        print("========== PrintJobTable ==========")
        print("self.UniqueJobTrackListCondor", self.UniqueJobTrackListCondor)
        print("UniqueJobTrackListSLURM", self.UniqueJobTrackListSLURM)

        # First lets clear the table so that everything prints out properly
        self.JobTable.clearContents()
        # pass the Job info to the separate Job Window so it can be displayed
        self.w.PrintTableWindow(self.UniqueJobTrackListCondor, self.UniqueJobTrackListSLURM)


        # Print Condor (LXPLUS) Jobs
        for i in range(len(self.UniqueJobTrackListCondor)):
            for j in range(len(self.UniqueJobTrackListCondor[i])):
                self.JobTable.setItem(i, j, QTableWidgetItem(self.UniqueJobTrackListCondor[i][j]))
        print("Updated Condor Table!")
        # Print SLURM Jobs
        for i in range(len(self.UniqueJobTrackListSLURM)):
            for j in range(len(self.UniqueJobTrackListSLURM[i])):
                #print("i", i)
                #print("j", j)
                self.JobTable.setItem(i + len(self.UniqueJobTrackListCondor), j, QTableWidgetItem(self.UniqueJobTrackListSLURM[i][j]))
        print("Updated SLURM Table!")

        # Lets update the History file here as well
        # Note, if the Job was submitted outside of this GUI, then we won't know the name of the CST file as
        # the LXPLUS and SLURM outputs do not make this (easily) accessible
        # We only want to read in the "old" jobs here
        if self.OldJobsHistoryReadCheck == 0:
            for i in self.UniqueJobTrackListCondor:
                if i[0] not in self.Historytableread: # check if job is already in the History file
                    self.Historytableread.append([str(i[0]),'LXPLUS','Unknown',i[1]])
            for i in self.UniqueJobTrackListSLURM:
                if i[0] not in self.Historytableread: # check if job is already in the History file
                    self.Historytableread.append([str(i[0]),'SLURM','Unknown',i[1]])
            self.JobInfoHistoryFile = open(self.JobInfoFileDir, 'w')
            self.JobInfoHistoryFile.write(tabulate(self.Historytableread, headers="firstrow",floatfmt="10.0f"))
            self.JobInfoHistoryFile.close()
            self.OldJobsHistoryReadCheck = 1


        #self.ShowOutputButton.setEnabled(True)
        #if self.w is not None:
            ## This will update the output window with every table update once the window has been opened
            #self.w.PrintSLURMOutput()

        time.sleep(1)
        #if self.ShowOutputButton.isChecked() == True:
        #    self.show_new_window()
        #    print("outwindow shown")
        #    self.w.PrintSLURMOutput()

        # We will now update the SLURM output in the Tabs area (If the user wants to!)
        if self.SLURMOutputOption.isChecked() == True:
            self.UpdateSLURMJobOutputTabs()

        # Here we will check if any of the Jobs are finished and will send the User an email to tell them
        # First need to check if an email has already been sent for each job

        print("Before self.EmailBeenSentCheckCondor: ", self.EmailBeenSentCheckCondor)
        print("Before self.EmailBeenSentCheckSLURM: ", self.EmailBeenSentCheckSLURM)
        print("self.UniqueJobTrackListCondor: ", self.UniqueJobTrackListCondor)
        print("self.UserEmail: ",self.UserEmail)
        if self.UserEmail != '':
            print("here10")
            for i in range(len(self.UniqueJobTrackListCondor)):
                print("here10.1")
                print(self.UniqueJobTrackListCondor[i][2], self.EmailBeenSentCheckCondor[i][1])
                if self.UniqueJobTrackListCondor[i][2] == 'Finished' and self.EmailBeenSentCheckCondor[i][1] == 0:
                    EmailObj = EmailSender.Emailer(self.UserEmail)
                    print("here11")
                    EmailObj.SendMessage("This message has been sent from the LXPLUS Python GUI.","Your Job " + self.UniqueJobTrackListCondor[i][0] + " has finished on the LXPLUS/CONDOR server.")
                    # This will stop the email from being sent again in the next checks
                    print("Email sent for Job: ", self.UniqueJobTrackListCondor[i])
                    self.EmailBeenSentCheckCondor[i][1] = 1

            for i in range(len(self.UniqueJobTrackListSLURM)):
                print("here10.2")
                if self.UniqueJobTrackListSLURM[i][2] == 'Finished' and self.EmailBeenSentCheckSLURM[i][1] == 0:
                    print("Job " + self.UniqueJobTrackListSLURM[i][0] + " has finished but email not yet sent")
                    EmailObj = EmailSender.Emailer(self.UserEmail)
                    EmailObj.SendMessage("This message has been sent from the LXPLUS Python GUI.","Your Job " + self.UniqueJobTrackListSLURM[i][0] + " has finished on the SLURM server.")
                    # This will stop the email from being sent again in the next checks
                    print("Email sent for Job: ", self.UniqueJobTrackListSLURM[i])
                    self.EmailBeenSentCheckSLURM[i][1] = 1

        print("After self.EmailBeenSentCheckCondor: ", self.EmailBeenSentCheckCondor)
        print("After self.EmailBeenSentCheckSLURM: ", self.EmailBeenSentCheckSLURM)

    def UpdateJobTableTimer(self):
        # The code cannot constantly query the Jobs on each cluster due to the response times of the SSH output
        # Also, if the code is constantly doing something, this makes it more likely that the GUI will crash if the user
        # tries to do something else (move window, submit another job, etc).
        # Therefore we put the Update functions on a timer, i.e. there is some space between checks.
        # We also define a boolean function which is checked by the other functions, and defines whether the code is
        # currently working on updating the Job Table or not. If the code is updating the Table, then the other functions
        # will wait until the Table has finished updating.
        # Theoretically this could be done via multiple threads but this becomes complicated quickly.
        # This could be implimented sometime in the future but this works for the time being and is the safer option
        print("UpdateJobTableTimer() starting")
        self.TableUpdateStarted = 1
        # make initial empty list of job IDS (unique) to keep track of things
        self.JobIDTrackListCondor = []
        self.UniqueJobTrackListCondor = []
        self.JobIDTrackListSLURM = []
        self.UniqueJobTrackListSLURM = []
        time.sleep(5)
        while True:
            self.JobStatusLabel.setText("Updating Table")
            # Check the LXPLUS Jobs
            self.UpdateJobTableCondor()
            # Check the SLURM Jobs
            self.UpdateJobTableSLURM()
            # Output the found Jobs to the Table
            self.PrintJobTable()
            # Signal that the Table has been updated for the first time
            self.First_Update_Check = 1

            # Once the Jobs Table has been updated, we want to check if any more Jobs have been added to the Queue
            self.JobQueueSubmit()
            time.sleep(10)


class CondorTailWindow(QWidget):
    """
    This "window" is a QWidget. If it has no parent,
    it will appear as a free-floating window.
    """
    #This class will be used to peek at the output of the chosen simulation

    def __init__(self, JobID,server):
        super().__init__()
        # Here we initiate the different areas on the GUI window
        self.JobID = JobID
        self.server = server
        self.setWindowTitle('LXPLUS CST Output: ' + JobID)
        self.setGeometry(600, 400, 1000, 400)
        self.OutputArea()
        self.hostlxplus = "lxplus"
        self.hostslurm = "hpc-batch.cern.ch"
        self.updatedstarted = 0



        # Add the Box widgets to the window
        self.gridAll = QGridLayout()
        self.gridAll.addWidget(self.OutputBox, 0,0)

        self.setLayout(self.gridAll)

        self.threadpool = QThreadPool()
        workerOutput = Worker(self.PeekAtOutput)
        self.threadpool.start(workerOutput)


    def OutputArea(self):
        self.OutputBox = QGroupBox("Output")
        self.Output = QTextBrowser(self.OutputBox)


        OutputGrid= QGridLayout()
        OutputGrid.addWidget(self.Output,0,0)
        #TableGrid.addWidget(self.ShowOutputButton, 1, 0)
        self.OutputBox.setLayout(OutputGrid)

    def PeekAtOutput(self):
        print("Opened Peeking Connection!")
        if self.server == 'LXPLUS':
            self.peekconnection = OpenPutty.ssh(self.hostlxplus, username, password, None)
            self.peekconnection.open_shell()
            self.peekconnection.send_shell('condor_tail -f ' + self.JobID)
            # condor_tail -f automatically updates so we dont need to resend the command
        elif self.server == 'SLURM':
            self.peekconnection = OpenPutty.ssh(self.hostslurm, username, password, None)
            self.peekconnection.open_shell()

        while self.isVisible():
            print('Window is visible')

            #self.Output.clear()
            SubmitTime = datetime.now()
            current_time = SubmitTime.strftime("%H:%M:%S")
            if self.updatedstarted == 1:
                print('CondorTailWindow Clearing Text')
                self.Output.clear()
            self.updatedstarted = 1
            self.Output.append("Last Updated: " + current_time)
            print('CondorTailWindow Here 1')

            if self.server == 'LXPLUS':
                # First clear the TextBrowser so theres not too much text!
                #self.Output.clear()
                print('CondorTailWindow Here 2')
                if len(self.peekconnection.strdata.split("\\n")) < 100:
                    print('CondorTailWindow Here 3')
                    updatelen = len(self.peekconnection.strdata.split("\\n"))
                else:
                    print('CondorTailWindow Here 4')
                    updatelen = 100
                print(f"updatelen: {updatelen}")
                for line in self.peekconnection.strdata.split("\\n")[-updatelen:]:
                    self.Output.append(line)
            elif self.server == 'SLURM':
                # First clear the TextBrowser so theres not too much text!
                self.Output.clear()
                self.peekconnection.send_shell('cat slurm-' + self.JobID + '.out')
                time.sleep(5)

                if len(self.peekconnection.strdata.split("\\n")) < 100:
                    updatelen = len(self.peekconnection.strdata.split("\\n"))
                else:
                    # Lets take the last 100 lines of the SLURM output?
                    updatelen = 100
                for line in self.peekconnection.strdata.split("\\n")[-updatelen:]:
                    self.Output.append(line)

            time.sleep(180)


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

#blockPrint()
#print(QStyleFactory.keys())
app = QApplication([])
app.setStyle('Fusion')
window = Window()
window.show()
app.exec()