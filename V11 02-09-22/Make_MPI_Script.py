'''
Aaron Farricker (CERN BE/RF-BR) 03-12-2019
This script creates the scripts to submit and run CST on the SLURM Cluster
'''
import yaml

class MPIScript:
    RunScriptName = ''
    cfg = None
    FileDirc = ''
    yamlDirc = ''
    RunScript = None

    def __init__(self, yamlDirc1):
        self.yamlDirc = yamlDirc1
        # self.yamlDirc = r"C:\Users\mssulliv\cernbox\Documents\HTCondor_Test_files\Setup.yaml"
        yamlDircArray = self.yamlDirc.split("\\")
        yamlDircArray.pop()
        self.FileDirc = ''
        for i in yamlDircArray:
            self.FileDirc += i + "\\"

    def loadyaml(self):
        with open(self.yamlDirc, 'r') as ymlfile:
            self.cfg = yaml.safe_load(ymlfile)
        print("yaml file loaded.")

    def MakeRunScript(self,cstvers):
        print("Script_Here1")
        try:
            if self.cfg['Solver'] == 'Eigenmode':
                self.CSTOption = '-m -e '
            elif self.cfg['Solver'] == 'Wakefield':
                self.CSTOption = '-t -tw '
            elif self.cfg['Solver'] == 'TimeDomain':
                self.CSTOption = '-t -r '
            elif self.cfg['Solver'] == 'FrequencyDomain':
                self.CSTOption = '-m -f '
            elif self.cfg['Solver'] == 'Schematic':
                self.CSTOption = '-c '
        except:
            print("No Solver specified, using wakefield")
            self.CSTOption = '-t -tw '

        self.RunScript=['#!/bin/bash',
                   '#SBATCH -p '+self.cfg['Queue'],
                   '#SBATCH -t '+str(self.cfg['RT Days'])+'-'+str(self.cfg['RT Hrs'])+':'+str(self.cfg['RT Mins']),
                   '#SBATCH -N '+str(self.cfg['Nnode']),
                   '#SBATCH -n '+str(self.cfg['Nnode']),
                   '#SBATCH --exclusive',
                   '\n',
                   'cp '+str(self.cfg['CST Path'])+str(self.cfg['Run File']) + './',
                   'HOSTSFILE=.hostlist-job$SLURM_JOB_ID',
                   'srun hostname -f > $HOSTSFILE',
                   'if [ "$SLURM_PROCID" == "0" ]; then',
                   '\t/afs/cern.ch/project/parc/' + cstvers + '/cst_design_environment --machinefile $HOSTSFILE ' + self.CSTOption + ' --withmpi '+self.cfg['Run File'],
                   '\trm -f $HOSTSFILE',
                   '\ttar -cvf '+str(self.cfg['Run File'][:-4])+'.tar '+str(self.cfg['Run File'][:-4])+'/ '+str(self.cfg['Run File']),
                   '\tcp '+str(self.cfg['Run File'][:-4])+'.tar '+str(self.cfg['CST Res'])+str(self.cfg['Run File'][:-4])+'.tar',
                   'fi'

                ]
        print("Script_Here2")
        self.RunScriptName = self.cfg['Run File'][:-4] + '_Script.sh'
        with open(self.FileDirc + self.cfg['Run File'][:-4]+'_Script.sh', 'w') as file:
            for i in self.RunScript:
                file.write('%s\n' % i)
        print("Script_Here3")
        print("self.CSTOption", self.CSTOption)

