#!/bin/bash
export EOS_MGM_URL=root://eosuser.cern.ch
export HOME=/afs/cern.ch/user/m/mssulliv/private/
eos cp /eos/user/m/mssulliv/Documents/HTCondor_Test_files/Collimator.cst ./Collimator.cst
/afs/cern.ch/project/parc/cst2020/cst_design_environment -t -tw ./Collimator.cst
tar -cvf ./Collimator.tar ./Collimator ./Collimator.cst
eos cp ./Collimator.tar /eos/user/m/mssulliv/Documents/HTCondor_Test_files/results/Collimator.tar
exit 0
