import os

from PyQt6.QtWidgets import QLabel, QApplication, QWidget, QPushButton, QGridLayout, QFileDialog, QGroupBox, QHBoxLayout, QVBoxLayout
import sys
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QLineEdit, QTextBrowser, QComboBox, QErrorMessage, QMainWindow,QTableWidget,QTableWidgetItem, QCheckBox
from PyQt6 import QtCore
from PyQt6.QtCore import QRunnable, pyqtSlot, QThreadPool
import Make_Scripts, OpenPutty, time, EmailSender,Make_MPI_Script
from datetime import datetime, timedelta
from collections import OrderedDict
import yaml

# The Worker class allows us to run processes on multiple threads so that the GUI doesnt crash when the code
# is busy doing something else
class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''
        self.fn(*self.args, **self.kwargs)

class Window(QWidget):

    def __init__(self):
        QWidget.__init__(self)
        # here we define various variables that will be used in the code
        self.SetupFileName = ''
        self.CSTFileName = ''
        self.loginCheck = 0
        self.ClusterID = ''
        self.JobFinishedCheck = 0
        self.JobExecutingCheck = 0
        self.ScriptsObj = None
        self.TableUpdateStarted = 0
        self.CodeBusyCheck = 0
        self.First_Update_Check = 0
        self.UserEmail = ''
        self.ServerChoice = ''
        self.EmailBeenSentCheckCondor = []
        self.EmailBeenSentCheckSLURM = []
        self.CancelledJobs = []
        self.setWindowTitle('LXPLUS CST Submission')
        self.setWindowIcon(QIcon("CERN-Logo.png"))
        #self.setFixedHeight(200)
        #self.setFixedWidth(200)

        # Here we initiate the different areas on the GUI window
        self.setGeometry(500,300,1000,400)
        self.UserPassArea()
        self.SelectFileArea()
        self.MessagesArea()
        self.ActionsArea()
        self.OptionsArea()
        self.JobTableArea()
        self.CancelJobArea()

        # Add the Box widgets to the window
        self.gridAll = QGridLayout()
        self.gridAll.addWidget(self.UserPassBox,0,0)
        self.gridAll.addWidget(self.SelectFileBox, 0, 1)
        self.gridAll.addWidget(self.MessagesBox, 1, 0,1,2)
        self.gridAll.addWidget(self.CancelBox, 2, 0)
        self.gridAll.addWidget(self.ActionsBox, 3, 0)
        self.gridAll.addWidget(self.OptionsBox, 0, 2)
        self.gridAll.addWidget(self.JobTableBox, 1, 2)
        self.gridAll.addWidget(self.CancelBox, 1, 2)
        self.gridAll.setRowStretch(5, 2)
        self.gridAll.setColumnStretch(5, 2)

        self.setLayout(self.gridAll)

        #Create the settings file when the window starts up
        self.SettingsFileDir = r"LXPLUS_SLURM_GUI_Settings.yaml"
        #assert os.path.isfile(self.SettingsFileDir)
        if not os.path.isfile(self.SettingsFileDir):
            self.SettingsFile = open(self.SettingsFileDir, 'w')
            self.SettingsFile.close()
        else:
            # If a Settings file already exists, lets read it in
            with open(self.SettingsFileDir, 'r') as ymlfile:
                self.LoadedSettings = yaml.safe_load(ymlfile)
                print(self.LoadedSettings)
            if not self.LoadedSettings == None:
                self.useredit.setText(self.LoadedSettings['Username'])
                self.passedit.setText(self.LoadedSettings['Password'])
                self.Emailedit.setText(self.LoadedSettings['Email'])
                self.Messages.append("Previous settings loaded.")
                self.RememberLoginCheck.setChecked(True)

        print("Settings File has been created in: ", os.getcwd())
        self.Messages.append("Settings File has been created in: " + os.getcwd())


    def UserPassArea(self):
        # This area defines the box where the user enters their username and password
        self.UserPassBox = QGroupBox("Enter Username and Password")
        self.UserPassBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        userlabel = QLabel('Username:')
        pwlabel = QLabel('Password:')
        self.useredit = QLineEdit()
        self.passedit = QLineEdit()
        self.passedit.setEchoMode(QLineEdit.EchoMode.Password)

        UPgrid = QGridLayout()
        UPgrid.addWidget(userlabel, 0, 0)
        UPgrid.addWidget(self.useredit, 0, 1)
        UPgrid.addWidget(pwlabel, 1, 0)
        UPgrid.addWidget(self.passedit, 1, 1)

        self.EmailLabel = QLabel("Send Email when Job Finished?:")
        self.Emailedit = QLineEdit()

        UPgrid.addWidget(self.EmailLabel, 2, 0)
        UPgrid.addWidget(self.Emailedit, 2, 1)

        UserLoginBtn = QPushButton('Login')
        UserLoginBtn.clicked.connect(self.UserAreaLogin)
        UPgrid.addWidget(UserLoginBtn, 4, 0)

        self.RememberLoginCheck = QCheckBox()
        self.RememberLoginCheck.setText("Remember my login details")

        UPgrid.addWidget(self.RememberLoginCheck , 3, 0)

        self.UserPassBox.setLayout(UPgrid)

    def UserAreaLogin(self):
        # When the "login" button is clicked, this function is executed

        self.username = self.useredit.text()
        self.password = self.passedit.text()
        self.UserEmail = self.Emailedit.text()

        self.loginCheck = 1
        time.sleep(1)
        self.host = "lxplus"

        self.connectionTemp = OpenPutty.ssh(self.host, self.username, self.password, None)
        print(self.connectionTemp)
        if self.connectionTemp.loginFail == 1:
            print("Login attempt failed")
            self.Messages.append('Login attempt failed - Please try again!')
            return
        print("test4")
        self.Messages.append('Login attempt successful!')
        self.connectionTemp.open_shell()
        self.connectionTemp.send_shell('kinit ' + self.username)
        time.sleep(1)
        self.connectionTemp.send_shell(self.password)
        self.Messages.append('Kerboros Token requested.')
        time.sleep(1)
        print(self.connectionTemp.strdata.split("\\n"))
        self.connectionTemp.close_connection()


        # If the "Remember my login" checkbox is checked, lets save a "settings" file to the local PC so store this info
        if self.RememberLoginCheck.isChecked() == True:
            print("Saving login details to " + self.SettingsFileDir)
            self.Messages.append("Saving login details to " + self.SettingsFileDir)
            with open(self.SettingsFileDir, 'w') as file:
                file.write("Username: " + self.username + "\n")
                file.write("Password: " + self.password + "\n")
                file.write("Email: " + self.UserEmail + "\n")
        else:
            # User does not want settings saved so rewrite the settings file
            with open(self.SettingsFileDir, 'w') as file:
                file.close()


    def SelectFileArea(self):
        # This area defines the box where the user selects their setup and CST files
        self.SelectFileBox = QGroupBox("Select Files:")
        self.SelectFileBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.SetupFileDialogButton = QPushButton('Setup File')
        self.SetupFileDialogButton.clicked.connect(self.SelectSetupFileButtonClick)

        self.CSTFileDialogButton = QPushButton('CST File')
        self.CSTFileDialogButton.clicked.connect(self.SelectCSTFileButtonClick)

        self.SetupFileNameLabel = QLabel()
        self.CSTFileNameLabel = QLabel()

        SFgrid = QGridLayout()

        SFgrid.addWidget(self.SetupFileDialogButton, 0,0)
        SFgrid.addWidget(self.CSTFileDialogButton, 1, 0)
        SFgrid.addWidget(self.SetupFileNameLabel, 0, 1)
        SFgrid.addWidget(self.CSTFileNameLabel, 1, 1)

        self.SelectFileBox.setLayout(SFgrid)

    def SelectSetupFileButtonClick(self):
        self.SetupFileName = QFileDialog.getOpenFileName(parent=self, directory=os.getcwd(),filter='*.yaml')[0]
        self.SetupFileNameLabel.setText('Setup file: ' + self.SetupFileName)
        print(self.SetupFileName)

    def SelectCSTFileButtonClick(self):
        self.CSTFileName = QFileDialog.getOpenFileName(parent=self, directory=os.getcwd(),filter='*.cst')[0]
        self.CSTFileNameLabel.setText('CST file: ' + self.CSTFileName)
        print(self.CSTFileName)

    def MessagesArea(self):
        # This area defines the box where messages are outputted
        # we add text to the message box by self.Messages.append()
        self.MessagesBox = QGroupBox("Messages")
        self.SelectFileBox.setGeometry(QtCore.QRect(20, 30, 211, 91))
        self.Messages = QTextBrowser(self.MessagesBox)
        self.Messages.setGeometry(QtCore.QRect(10, 90, 331, 111))
        MessGrid= QGridLayout()
        MessGrid.addWidget(self.Messages,0,0)
        self.MessagesBox.setLayout(MessGrid)

    def ActionsArea(self):
        # This area defines the box where the action buttons are placed
        self.ActionsBox = QGroupBox('Actions')
        self.ActionsBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.SubmitButton = QPushButton('Submit Job')

        self.MakeScriptsOnlyButton = QPushButton('Make Scripts only')
        self.UpdateTableOnlyButton = QPushButton('Update Table only')

        self.SubmitButton.clicked.connect(self.SubmitButtonClick)
        self.MakeScriptsOnlyButton.clicked.connect(self.MakeScriptsOnlyButtonClick)

        self.UpdateTableOnlyButton.clicked.connect(self.UpdateTableOnlyButtonClick)

        ActionsGrid = QGridLayout()
        ActionsGrid.addWidget(self.SubmitButton, 0,0)
        #ActionsGrid.addWidget(self.CancelButton, 0, 1)
        ActionsGrid.addWidget(self.MakeScriptsOnlyButton, 0, 2)
        ActionsGrid.addWidget(self.UpdateTableOnlyButton, 0, 3)

        self.ActionsBox.setLayout(ActionsGrid)

    def CancelJobArea(self):
        self.CancelBox = QGroupBox('Cancel Jobs')
        self.CancelBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.CancelLXPLUSLabel = QLabel("Job to cancel (LXPLUS):")
        self.CancelSLURMLabel = QLabel("Job to cancel (SLURM):")
        self.CancelLXPLUSCB = QComboBox()
        self.CancelSLURMCB = QComboBox()
        self.CancelLXPLUSButton = QPushButton('Cancel Job (LXPLUS)')
        self.CancelSLURMButton = QPushButton('Cancel Job (SLURM)')

        self.CancelLXPLUSButton.clicked.connect(self.CancelLXPLUSButtonClick)
        self.CancelSLURMButton.clicked.connect(self.CancelSLURMButtonClick)

        # We will need to dynamically update the combo box with the current jobs

        CancelGrid = QGridLayout()
        CancelGrid.addWidget(self.CancelLXPLUSLabel, 0, 0)
        CancelGrid.addWidget(self.CancelLXPLUSCB, 0, 1)
        CancelGrid.addWidget(self.CancelLXPLUSButton, 0, 2)
        CancelGrid.addWidget(self.CancelSLURMLabel, 1, 0)
        CancelGrid.addWidget(self.CancelSLURMCB, 1, 1)
        CancelGrid.addWidget(self.CancelSLURMButton, 1, 2)

        self.CancelBox.setLayout(CancelGrid)

    def MakeScriptsOnlyButtonClick(self):

        # First check that the setup and CST files have been selected
        if self.SetupFileName == '' or self.CSTFileName:
           print(self.SetupFileName)
           error_dialog = QErrorMessage(self)
           error_dialog.setWindowTitle("Error!")
           error_dialog.showMessage('Error: Setup or CST file not selected!')
        else:
            print('Making Scripts')
            print(self.SetupFileName)
            print(self.SetupFileName.replace('/','\\'))

            if self.CSTCB.currentIndex() == 0:
                self.cstvers = 'cst2020'
            elif self.CSTCB.currentIndex() == 1:
                self.cstvers = 'cst2021'

            self.ServerChoice = self.ServerOptionCB.currentText()
            if self.ServerChoice == 'LXPLUS':
                self.ScriptsObj = Make_Scripts.scripts(self.SetupFileName.replace('/', '\\'))
                self.ScriptsObj.loadyaml()
                self.ScriptsObj.MakeSubFile(self.cstvers)
                self.ScriptsObj.MakeCSTScrpt(self.cstvers)
                self.Messages.append('Scripts have been created in ' + self.ScriptsObj.FileDirc)
            elif self.ServerChoice == 'SLURM':
                self.ScriptsObjSLURM = Make_MPI_Script.MPIScript(self.SetupFileName.replace('/', '\\'))
                self.ScriptsObjSLURM.loadyaml()
                self.ScriptsObjSLURM.MakeRunScript(self.cstvers)
                self.Messages.append('Scripts have been created in ' + self.ScriptsObjSLURM.FileDirc)



    def UpdateTableOnlyButtonClick(self):
        # This allows us to update the Job table without having to submit a job
        # We need to make connections to both the LXPLUS and SLURM clusters in order to
        # query them for the status of the running jobs

        self.host = "lxplus"
        print("Trying to connect")
        self.connection = OpenPutty.ssh(self.host, self.username, self.password, None)
        self.connection.open_shell()

        print("Connecting to server on ip: " + str(self.host) + ".")

        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")

        self.host = "hpc-batch.cern.ch"
        print("Trying to connect")
        self.connectionSLURM = OpenPutty.ssh(self.host, self.username, self.password, None)
        self.connectionSLURM.open_shell()

        print("Connecting to server on ip: " + str(self.host) + ".")

        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")

        # Now we run the UpdateJobTableTimer function on a different thread so it can
        # work in the background
        Tableworker = Worker(self.UpdateJobTableTimer)
        self.threadpoolTable = QThreadPool()
        self.threadpoolTable.start(Tableworker)


    def SubmitExecute(self):
        # This submits a Job to the LXPLUS cluster!!
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO EXECUTE CONDOR JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1
        self.UserEmail = self.Emailedit.text()
        # Here are the details that LXPLUX needs to sign in and connect
        self.host = "lxplus"
        port = 22
        self.initial = self.username[0]

        if self.CSTCB.currentIndex() == 0:
            self.cstvers = 'cst2020'
        elif self.CSTCB.currentIndex() == 1:
            self.cstvers = 'cst2021'

        # We create a script object to handle the creation of the LXPLUS scripts
        self.ScriptsObj = Make_Scripts.scripts(self.SetupFileName.replace('/', '\\'))
        self.ScriptsObj.loadyaml()
        self.ScriptsObj.MakeSubFile(self.cstvers)
        self.ScriptsObj.MakeCSTScrpt(self.cstvers)
        print(self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][:-4] + '_Log.txt')

        print('==========================')
        print('Setup details: ')
        print('CPUs requested: ' + str(self.ScriptsObj.cfg['NProc']))
        print('==========================')

        self.Messages.append('==========================')
        self.Messages.append('Setup details: ')
        self.Messages.append('CPUs requested: ' + str(self.ScriptsObj.cfg['NProc']))
        self.Messages.append('==========================')
        self.connection = OpenPutty.ssh(self.host, self.username, self.password, self.ScriptsObj)

        print("Connecting to server on ip: " + str(self.host) + ".")
        print("Waiting to be submitted...")
        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")
        self.Messages.append("Waiting to be submitted...")


        # This function from OpenPutty.py sends the files over to the LXPLUS local directory and then submits them
        self.connection.execute_scripts()

        print('==========================')
        self.Messages.append('==========================')
        time.sleep(2)
        # Wait until we get a response from the LXPLUS cluster that the job has been submitted
        while "submitted to cluster" not in self.connection.strdata.split("\\n")[-2]:
            time.sleep(1)
        SubmitTime = datetime.now()
        current_time = SubmitTime.strftime("%H:%M:%S")
        print("Job submitted at " + current_time)
        self.Messages.append("Job submitted at " + current_time)

        #retrive the JOB ID of the submitted job
        self.ClusterID = self.connection.strdata.split("\\n")[-2].split()[-1].split(".")[0]

        self.host = "hpc-batch.cern.ch"
        print("Trying to connect")
        # Now we open a connection to the SLURM Cluster ready for the jobs to be queried later
        self.connectionSLURM = OpenPutty.ssh(self.host, self.username, self.password, None)
        self.connectionSLURM.open_shell()

        print("Connecting to server on ip: " + str(self.host) + ".")

        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")



        print("ClusterID: ", self.ClusterID)
        self.Messages.append('ClusterID: ' + self.ClusterID)

        print('Waiting for Job to start running...')
        self.Messages.append('Waiting for Job to start running...')

        # Once the Job is finished, need to reset everything so another Job can be submitted.
        self.JobFinishedCheck = 0
        self.JobExecutingCheck = 0
        self.Messages.append(
            'Writing log file to: ' + self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][
                                                                        :-4] + '_Log.txt')
        self.CodeBusyCheck = 0
        # Job has started running so start updating the Job Table (if not already running)
        if self.TableUpdateStarted == 0:
            # we will 'click' the UpdateTableOnly button as this opens the shells to both clusters
            # so that they may be read
            self.UpdateTableOnlyButtonClick()

    def SubmitExecuteSLURM(self):
        # This submits a Job to the SLURM cluster!!
        # This is very similar to the SubmitExecute() function for the LXPLUS cluster but with minor differences
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO EXECUTE SLURM JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1

        self.UserEmail = self.Emailedit.text()
        # Here are the details that SLURM needs to sign in and connect
        self.host = "hpc-batch.cern.ch"
        self.port = 22
        self.initial = self.username[0]

        if self.CSTCB.currentIndex() == 0:
            self.cstvers = 'cst2020'
        elif self.CSTCB.currentIndex() == 1:
            self.cstvers = 'cst2021'

        self.ScriptsObjSLURM = Make_MPI_Script.MPIScript(self.SetupFileName.replace('/', '\\'))
        self.ScriptsObjSLURM.loadyaml()
        self.ScriptsObjSLURM.MakeRunScript(self.cstvers)


        print('==========================')
        print('Setup details: ')
        print('Nodes requested: ' + str(self.ScriptsObjSLURM.cfg['Nnode']))
        print('==========================')

        self.Messages.append('==========================')
        self.Messages.append('Setup details: ')
        self.Messages.append('Nodes requested: ' + str(self.ScriptsObjSLURM.cfg['Nnode']))
        self.Messages.append('==========================')

        time.sleep(1)

        print("Connecting to server on ip: " + str(self.host) + ".")
        print("Waiting to be submitted...")
        self.Messages.append("Connecting to server on ip: " + str(self.host) + ".")
        self.Messages.append("Waiting to be submitted...")

        self.connectionSLURM = OpenPutty.ssh(self.host, self.username, self.password, self.ScriptsObjSLURM)
        self.connectionSLURM.open_shell()
        # SLURM requires a password when you first open the connection =
        self.connectionSLURM.send_shell(self.password)
        self.connectionSLURM.execute_scripts_SLURM()

        time.sleep(5)
        print(self.connectionSLURM.strdata.split("\\n"))

        print('==========================')
        self.Messages.append('==========================')
        time.sleep(2)

        SubmitTime = datetime.now()
        current_time = SubmitTime.strftime("%H:%M:%S")
        print("Job submitted at " + current_time)
        self.Messages.append("Job submitted at " + current_time)

        # Look at the last 10 lines of the SLURM output to find the submitted JOB ID
        linecount = 0
        for i in self.connectionSLURM.strdata.split("\\n")[-10:]:
            #print("i = ", linecount, i)
            if "Submitted batch job" in i:
                #print(i)
                #print(i.split())
                #print(i.split()[-1].replace('\\r',''))
                self.ClusterID = i.split()[-1].replace('\\r','')
            linecount = linecount + 1

        self.host = "hpc-batch.cern.ch"

        # Job has started running so start updating the Job Table
        print("self.TableUpdateStarted: ", self.TableUpdateStarted)


        print("ClusterID: ", self.ClusterID)
        self.Messages.append('ClusterID: ' + self.ClusterID)

        print('Waiting for Job to start running...')
        self.Messages.append('Waiting for Job to start running...')

        # Once the Job is finished, need to reset everything so another Job can be submitted.
        self.JobFinishedCheck = 0
        self.JobExecutingCheck = 0
        self.CodeBusyCheck = 0
        if self.TableUpdateStarted == 0:
            #we will 'click' the UpdateTableOnly button as this opens the shells to both clusters
            # so that they may be read
            self.UpdateTableOnlyButtonClick()
        #self.Messages.append(
        #    'Writing log file to: ' + self.ScriptsObj.FileDirc + '\\' + self.ScriptsObjSLURM.cfg['Run File'][
        #                                                                :-4] + '_Log.txt')


    def SubmitButtonClick(self):
        # When Submit Job is clicked, first need to check that the Setup and CST files are selected
        # If not --> throw error!
        self.UserEmail = self.Emailedit.text()
        if self.SetupFileName == '':
           print(self.SetupFileName)
           error_dialog = QErrorMessage(self)
           error_dialog.setWindowTitle("Error!")
           error_dialog.showMessage('Error: Setup file not selected!')
        elif self.CSTFileName == '':
            error_dialog = QErrorMessage(self)
            error_dialog.setWindowTitle("Error!")
            error_dialog.showMessage('Error: CST file not selected!')
        elif self.loginCheck == 0:
            error_dialog = QErrorMessage(self)
            error_dialog.setWindowTitle("Error!")
            error_dialog.showMessage('Error: Login details not entered!')
        else:
            while self.CodeBusyCheck == 1:
                # wait until the code isnt busy doing something else (to stop crashes)
                print("WAITING TO SUBMIT")
                time.sleep(1)

            self.ServerChoice = self.ServerOptionCB.currentText()

            self.Messages.append('Good to go!')
            print(self.ServerChoice)

            # Need to check which server has been selected and execute the appropriate function
            if self.ServerChoice == 'LXPLUS':
                self.Messages.append('Submitting job to LXPLUS Cluster')
                worker = Worker(self.SubmitExecute)
                self.threadpoolsubmit = QThreadPool()
                self.threadpoolsubmit.start(worker)
            elif self.ServerChoice == 'SLURM':
                self.Messages.append('Submitting job to SLURM Cluster')
                workerSLURM = Worker(self.SubmitExecuteSLURM)
                self.threadpoolsubmitSLURM = QThreadPool()
                self.threadpoolsubmitSLURM.start(workerSLURM)

    def OptionsArea(self):
        self.OptionsBox = QGroupBox('Options')
        self.OptionsBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.CSTCBLabel = QLabel("CST Version:")
        self.CSTCB = QComboBox()
        self.CSTCB.addItem("CST2020")
        self.CSTCB.addItem("CST2021")

        print(self.CSTCB.currentIndex())
        self.CSTCB.currentIndexChanged.connect(self.CSTSelection)

        self.ServerOptionLabel = QLabel("Use Server:")
        self.ServerOptionCB = QComboBox()
        self.ServerOptionCB.addItem("LXPLUS")
        self.ServerOptionCB.addItem("SLURM")

        OptionsGrid = QGridLayout()
        OptionsGrid.addWidget(self.CSTCBLabel,0,0)
        OptionsGrid.addWidget(self.CSTCB,0,1)
        OptionsGrid.addWidget(self.ServerOptionLabel, 2, 0)
        OptionsGrid.addWidget(self.ServerOptionCB, 2, 1)


        self.OptionsBox.setLayout(OptionsGrid)

    def CSTSelection(self):
        print(self.CSTCB.currentIndex())

    def CancelLXPLUSButtonClick(self):
        # This function allows the user to cancel a submitted job on the LXPLUS cluster
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO CANCEL CONDOR JOBS")
            # This will ONLY appear in the console
            time.sleep(1)

        self.CodeBusyCheck = 1
        self.command3 = "condor_rm " + self.CancelLXPLUSCB.currentText()
        self.connection.send_shell("\x03")
        time.sleep(1)
        self.connection.send_shell(self.command3)
        print("command " + self.command3 + " has been sent.")
        while "removal" not in self.connection.strdata.split("\\n")[-2]:
            # wait until the Job has been marked for removal in the LXPLUS cluster
            pass
        print("Job with ClusterID " + self.CancelLXPLUSCB.currentText() + " has been marked for removal.")
        self.Messages.append("Job with ClusterID " + self.CancelLXPLUSCB.currentText() + " has been marked for removal.")
        # We need to keep a record of the jobs that the user has manually cancelled so we can display this on the Job
        # Table. The LXPLUS cluster does not keep a record of this!
        self.CancelledJobs.append(self.CancelLXPLUSCB.currentText())
        self.CodeBusyCheck = 0

    def CancelSLURMButtonClick(self):
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO CANCEL SLURM JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1
        self.command4 = "scancel " + self.CancelSLURMCB.currentText()
        self.connectionSLURM.send_shell("\x03")
        time.sleep(1)
        self.connectionSLURM.send_shell(self.command4)
        print("command " + self.command4 + " has been sent.")
        #while "removal" not in self.connectionSLURM.strdata.split("\\n")[-2]:
        #    pass
        print("Job with ClusterID " + self.CancelSLURMCB.currentText() + " has been marked for removal.")
        self.Messages.append("Job with ClusterID " + self.CancelSLURMCB.currentText() + " has been marked for removal.")
        # We need to keep a record of the jobs that the user has manually cancelled so we can display this on the Job
        # Table. The SLURM cluster does not keep a record of this!
        self.CancelledJobs.append(self.CancelSLURMCB.currentText())
        self.CodeBusyCheck = 0

    def JobTableArea(self):
        # This function defines the area where the Job Table is displayed in the GUI
        self.JobTableBox = QGroupBox("Job Status")
        #self.JobTableBox.setGeometry(QtCore.QRect(20, 300, 511, 91))
        self.JobTableBox.setMinimumSize(400,400)
        self.JobTableBox.setGeometry(0, 0, 4000, 2000)
        self.JobTable = QTableWidget()
        #self.JobTable.setGeometry(QtCore.QRect(10, 90, 331, 111))
        self.JobTable.setGeometry(0, 0, 4000, 2000)

        self.JobTable.setRowCount(20)
        self.JobTable.setColumnCount(6)


        self.JobTable.setHorizontalHeaderLabels(['Job ID','Submission', 'State', 'Server', 'Partition', 'Nodes'])
        self.JobTable.horizontalHeader().setDefaultSectionSize(100)

        TableGrid= QGridLayout()
        TableGrid.addWidget(self.JobTable,0,0)
        self.JobTableBox.setLayout(TableGrid)

    def UpdateJobTableCondor(self):
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO UPDATE CONDOR JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1
        print("UpdateJobTable() starting")
        self.allJobsListCondor = []
        print("here0")

        # send command to the LXPLUS Cluster in order to read the Job output
        self.connection.send_shell('condor_q')

        time.sleep(5)

        # need to find the latest update in the output
        updateposlist = []
        updateposindex = 0
        updateposchecks = 0
        while updateposlist == []:
            for i in self.connection.strdata.split("\\n"):
                #print("updateposindex: ", updateposindex, ", i: ", i)
                if "OWNER" in i:
                    updateposlist.append(updateposindex)
                updateposindex = updateposindex + 1
            print("updateposlist: ", updateposlist)
            # need to reset index if no update has been found!
            if updateposlist == []:
                updateposindex = 0
                # lets keep track of how many times we have checked the output
                updateposchecks = updateposchecks + 1
            time.sleep(2)
            if updateposchecks == 20:
                # We have checked the output and cannot find the job list, print message and see whats happening
                print("updateposchecks = 20 and cant find the Jobs!")
                print("LXPLUS output: ", self.connection.strdata.split("\\n"))
                return
        latestupdatepos = updateposlist[-1] + 1
        print("latestupdatepos: ", latestupdatepos)

        print(self.connection.strdata.split("\\n"))
        print(self.connection.strdata.split("\\n")[latestupdatepos])
        # We have to read the output of the LXPLUS cluster to dynamically find the number of active jobs on the server
        #if self.connection.strdata.split("\\n")[latestupdatepos] == '\\r':
        if "1" not in self.connection.strdata.split("\\n")[latestupdatepos]:
            self.TotalJobNum = 0
            self.CodeBusyCheck = 0
            return
            # If there are no jobs found, then exit out of the UpdateJobTableCondor function
        else:
            totaljobsearch = 0
            while "1" in self.connection.strdata.split("\\n")[latestupdatepos + totaljobsearch]:
                # a "1" should appear in the line if there is a Job on the cluster, otherwise theres no job
                print("totaljobsearch: ", totaljobsearch)
                print(self.connection.strdata.split("\\n")[latestupdatepos + totaljobsearch])
                # if the next line is not '\\r' then there must be a job
                # increment through lines until '\\r' is found, counting the jobs as we go
                totaljobsearch = totaljobsearch + 1

            #Once the while loop is stopped, we have reached the end of the list of jobs so assign the TotalJobNum
            self.TotalJobNum = totaljobsearch

        print(self.TotalJobNum)
        print("here1")

        # For each job we want ["JOB ID", "SUBMISSION DATE/TIME", "STATUS"]
        for j in range(self.TotalJobNum):
            self.allJobsListCondor.append([])
            #Job_ID
            self.allJobsListCondor[j].append(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[-1].split('\\r')[0])

            # Submit date/time
            self.allJobsListCondor[j].append(self.connection.strdata.split("\\n")[latestupdatepos + j].split()[3] + " - " + self.connection.strdata.split("\\n")[latestupdatepos + j].split()[4])

            print("here2")
            # Here we look at the output and work out what the current state of the job is (Idle/Running, etc)
            if self.connection.strdata.split("\\n")[latestupdatepos + j].split()[7] == '1':
                # Job is IDLE
                self.allJobsListCondor[j].append('Idle')
            elif self.connection.strdata.split("\\n")[latestupdatepos + j].split()[6] == '1':
                # Job is RUNNING
                self.allJobsListCondor[j].append('Running')
            else:
                print("here.2.01")
                print("self.UniqueJobTrackListCondor: ", self.UniqueJobTrackListCondor)
                print("self.allJobsListCondor[j]: ", self.allJobsListCondor[j])
                print("self.UniqueJobTrackListCondor[j][0]: ", self.UniqueJobTrackListCondor[j][0])
                print("self.allJobsListCondor[j][0]: ", self.allJobsListCondor[j][0])
                # Sometimes if we catch the output just as the Job finishes, it will show as "finished" in the output
                # Otherwise the Job just completely disappears in the output
                for i in range(len(self.UniqueJobTrackListCondor)):
                    if self.UniqueJobTrackListCondor[i][0] != self.allJobsListCondor[j][0]:
                        print("Job " + self.allJobsListCondor[j][0] + " has finished!")
                        self.allJobsListCondor[j].append('Finished')
            print("here2.1")
            # Need to add to each Job that it belongs to the LXPLUS Cluster
            self.allJobsListCondor[j].append('LXPLUS')
            UniqueJobIDList = []
            for i in self.UniqueJobTrackListCondor:
                UniqueJobIDList.append(i[0])
            print("here2.2")

            if self.allJobsListCondor[j] not in self.UniqueJobTrackListCondor:
                if self.allJobsListCondor[j][0] in UniqueJobIDList:
                    print("here2.3")
                    # if the Job info is not unique but the Job ID is already stored, then the STATE of the job has updated (IDLE -> RUNNING)
                    # therefore we must update the entry in the UniqueJobTrackListCondor
                    print("self.UniqueJobTrackListCondor: ", self.UniqueJobTrackListCondor)
                    print("self.allJobsListCondor[j]: ", self.allJobsListCondor[j])
                    for i in self.UniqueJobTrackListCondor:
                        if i[0] in self.CancelledJobs:
                            # check if job has been cancelled by the user
                            i[2] = 'Cancelled'
                        elif i[0] == self.allJobsListCondor[j][0]:
                            i[2] = self.allJobsListCondor[j][2]

                    print("here2.31")
                    # New job has started so print message of the time
                    ExecuteTime = datetime.now()
                    current_time = ExecuteTime.strftime("%H:%M:%S")
                    print('Job: ' + self.allJobsListCondor[j][0] + ' started running at ' + current_time)
                    self.Messages.append('Job: ' + self.allJobsListCondor[j][0] + ' started running at ' + current_time)
                    print("here2.4")
                else:
                    print("here2.5")
                    # if the Job info isnt unique and the JobID isnt stored, then append to the Unique list
                    # so we can keep a track of all the Jobs that are running and those that disappear!
                    self.UniqueJobTrackListCondor.append(self.allJobsListCondor[j])
                    # The job has been added to the Unique list, so keep check if a Finished Email has been sent yet
                    self.EmailBeenSentCheckCondor.append([])
                    self.EmailBeenSentCheckCondor[j].append(self.UniqueJobTrackListCondor[j][0])
                    self.EmailBeenSentCheckCondor[j].append(0)
                    # We also need to add the job to the Cancel Combo Box so it can be canceled later
                    self.CancelLXPLUSCB.addItem(self.UniqueJobTrackListCondor[j][0])
                    print("here2.51")

        print("self.UniqueJobTrackListCondor[j][0]: ", self.UniqueJobTrackListCondor[j][0])
        print("self.CancelledJobs: ", self.CancelledJobs)
        for k in self.UniqueJobTrackListCondor:
            if k[0] in self.CancelledJobs:
                # check if the job has been cancelled by user
                k[2] = 'Cancelled'
                print("JOB ", k[0], " HAS BEEN CANCELLED")
        print("UniqueJobTrackListSLURM: ", self.UniqueJobTrackListCondor)

        print("here3")

        currentJobIDList = []
        for i in self.allJobsListCondor:
            currentJobIDList.append(i[0])
        print("Current Job list: ", currentJobIDList)

        print("here5")

        # Now we have to check if the stored unique Job Ids is in the SSH output -> if not, the job has finished
        for i in self.UniqueJobTrackListCondor:
            if i[0] not in currentJobIDList:
                # job ID has disappeared -> set to finished
                if i[2] == 'Finished':
                    pass
                else:
                    i[2] = 'Finished'
                    # Check if 'Finished' message has been printed already
                    finishTime = datetime.now().strftime("%H:%M:%S")
                    print('Job: ' + i[0] + ' finished at ' + finishTime)
                    self.Messages.append('Job: ' + i[0] + ' finished at ' + finishTime)
                    # The job is finished but we need to see if it was aborted or not!
                    print('i[0]: ',i[0] )
                    self.connection.send_shell('condor_wait -status log/cst.' + i[0].split('.')[0] + '.log')
                    time.sleep(1)
                    for k in self.connection.strdata.split("\\n")[latestupdatepos::-1]:
                        if i[0] in k:
                            if 'aborted' in k:
                                # Job has been aborted!
                                i[2] = 'Aborted'
                                aborttime = datetime.now().strftime("%H:%M:%S")
                                print('Job: ' + i[0] + ' was aborted by the LXPLUS Cluster at ' + aborttime)
                                self.Messages.append('Job: ' + i[0] + ' was aborted by the LXPLUS Cluster at ' + aborttime)

        # Now output the updated Unique Job info into the GUI Table
        print("here7")

        # Update the Logfile at the end of each Table Update
        if self.ScriptsObj != None:
            LogFile = open(self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][:-4] + '_Log.txt', "w")
            LogFile.write(self.Messages.toPlainText())
            LogFile.close()
        print("here8")

        # Sometimes when jobs 'finish' they can be aborted by the LXPLUS cluster
        # so it can be useful to see the error/log/output files on the AFS directory
        # Once job has finished, transfer these files to the users local PC
        if self.ScriptsObj != None:
            for i in self.UniqueJobTrackListCondor:
                if i[2] == 'Finished':
                    EOSOutPutDir = self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/' + 'LXPLUS_output/'
                    LocalOutPutDir = self.ScriptsObj.cfg['EOS Res'].replace('/eos/user/'+self.username[0]+'/'+self.username, 'C:/Users/'+self.username+'/cernbox') + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/' + 'LXPLUS_output/'
                    print('EOSOutPutDir: ', EOSOutPutDir)
                    print('LocalOutPutDir: ', LocalOutPutDir)
                    if not os.path.exists(LocalOutPutDir):
                        # if weve already moved the files, we dont want to do it again
                        print('Moving LXPLUS output files...')
                        # os.mkdir(EOSOutPutDir)
                        print('Creating Directory...')
                        # Move .tar file into a folder which specifies the time and ID of the Job for easier reading
                        self.connection.send_shell("mkdir " + self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/' + '\n' +
                                                   "cp " + self.ScriptsObj.cfg['EOS Res'] + self.ScriptsObj.cfg['CST File'][:-4] + '.tar ' + self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/'  "\n")
                        print("Here9")

                        MoveFinishedFilesCommand = "mkdir " + EOSOutPutDir + "\n" + \
                                                "cd " + self.ScriptsObj.cfg['AFS Dir'] + "error \n " + \
                                                "cp cst." + i[0] + ".err " + EOSOutPutDir + "\n" + \
                                               "cp cst." + i[0].split('.')[0] + ".err " + EOSOutPutDir + "\n" + \
                                               "cd " + self.ScriptsObj.cfg['AFS Dir'] + "log \n " + \
                                               "cp cst." + i[0] + ".log " + EOSOutPutDir + "\n" + \
                                               "cp cst." + i[0].split('.')[0] + ".log " + EOSOutPutDir + "\n" + \
                                               "cd " + self.ScriptsObj.cfg['AFS Dir'] + "output \n " + \
                                               "cp cst." + i[0] + ".out " + EOSOutPutDir + "\n" + \
                                               "cp cst." + i[0].split('.')[0] + ".out " + EOSOutPutDir + "\n"
                        print(MoveFinishedFilesCommand)

                        self.connection.send_shell(MoveFinishedFilesCommand)
                        self.Messages.append('Moved results and LXPlus log/error/out files to: ' + self.ScriptsObj.cfg['EOS Res'] + 'JobMade_' + self.ScriptsObj.ScriptMadeTime + '_JobID_' + i[0] + '/')

        self.CodeBusyCheck = 0

    def UpdateJobTableSLURM(self):
        # This function is very similar to the UpdateJobTableCondor function above with some minor differences
        while self.CodeBusyCheck == 1:
            # wait until the code isnt busy doing something else (to stop crashes)
            print("WAITING TO UPDATE SLURM JOBS")
            time.sleep(1)

        self.CodeBusyCheck = 1
        print("UpdateJobTable() SLURM starting")
        self.allJobsListSLURM = []
        print("here0")
        # Open new SSH terminal for q checks?
        # self.connection2 = OpenPutty.ssh(self.host, self.username, self.password, self.ScriptsObj)
        self.connectionSLURM.send_shell('squeue -u ' + self.username)
        print('squeue -u ' + self.username)
        time.sleep(5)

        # need to find the latest update in the output
        updateposlist = []
        updateposindex = 0
        while updateposlist == []:
            for i in self.connectionSLURM.strdata.split("\\n"):
                # print("updateposindex: ", updateposindex, ", i: ", i)
                if "JOBID" in i:
                    updateposlist.append(updateposindex)
                updateposindex = updateposindex + 1
            print("updateposlist: ", updateposlist)
            # need to reset index if no update has been found!
            if updateposlist == []:
                updateposindex = 0
            time.sleep(2)
        latestupdatepos = updateposlist[-1] + 1
        print("latestupdatepos (SLURM): ", latestupdatepos)

        print(self.connectionSLURM.strdata.split("\\n"))
        print(self.connectionSLURM.strdata.split("\\n")[latestupdatepos])
        if self.connectionSLURM.strdata.split("\\n")[latestupdatepos] == '\\r' or self.connectionSLURM.strdata.split("\\n")[latestupdatepos] == '\\r\\r':
            self.TotalJobNum = 0
            # If there are no jobs found, then exit out of the UpdateJobTableSLURM function
            self.CodeBusyCheck = 0
            return
        else:
            totaljobsearch = 0
            #while self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r'\r' and \
            #        self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r"'b'\r" and \
            #        self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r"\r\r" and \
            #        self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch] != r"'b'\r\r":
            while self.username in self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch]:
                # your username should appear in the line if you have a Job running, so check for this
                print("totaljobsearch: ", totaljobsearch)
                print(self.connectionSLURM.strdata.split("\\n")[latestupdatepos + totaljobsearch])
                # Read through lines until your username is no longer found
                totaljobsearch = totaljobsearch + 1

            # Once the while loop is stopped, we have reached the end of the list of jobs so assign the TotalJobNum
            self.TotalJobNum = totaljobsearch

        print(self.TotalJobNum)
        print("here1")

        # For each job we want ["JOB ID", "SUBMISSION DATE/TIME", "STATUS"] etc
        for j in range(self.TotalJobNum):
            self.allJobsListSLURM.append([])
            # sometimes there is a "'b'" at the start of the line so we need to check for this then fix it
            if self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split()[0] == "'b'":
                print("'b' detected!")
                #self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split().pop(0)
                SLURM_Line_Array = self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split()[1:]
            else:
                SLURM_Line_Array = self.connectionSLURM.strdata.split("\\n")[latestupdatepos + j].split()
            # Job_ID
            self.allJobsListSLURM[j].append(SLURM_Line_Array[0])

            # Submit date/time (time running)
            self.allJobsListSLURM[j].append(SLURM_Line_Array[5] + ' (time running)' )

            print("here2")
            # Here we work out what state each job is in by reading the SLURM output
            if SLURM_Line_Array[4] == 'PD':
                # Job is PENDING
                self.allJobsListSLURM[j].append('Pending')
            elif SLURM_Line_Array[4] == 'R':
                # Job is RUNNING
                self.allJobsListSLURM[j].append('Running')
            else:
                print("here.2.01")
                print("self.UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)
                print("self.allJobsListSLURM[j]: ", self.allJobsListSLURM[j])
                print("self.UniqueJobTrackListSLURM[i][0]: ", self.UniqueJobTrackListSLURM[j][0])
                print("self.allJobsListSLURM[j][0]: ", self.allJobsListSLURM[j][0])

                # We only want to compare against the Unique list AFTER the initial Table Update
                if self.First_Update_Check == 0:
                    for i in range(len(self.UniqueJobTrackListSLURM)):
                        if self.UniqueJobTrackListSLURM[i][0] != self.allJobsListSLURM[j][0]:
                            print("Job " + self.allJobsListSLURM[j][0] + " has finished!")
                            self.allJobsListSLURM[j].append('Finished')

            print("here2.1")
            self.allJobsListSLURM[j].append('SLURM')

            # Add the Partition the job is running on and the Nodes requested
            self.allJobsListSLURM[j].append(SLURM_Line_Array[1])
            self.allJobsListSLURM[j].append(SLURM_Line_Array[6])


            UniqueJobIDList = []
            for i in self.UniqueJobTrackListSLURM:
                UniqueJobIDList.append(i[0])
            print("here2.2")
            print("self.allJobsListSLURM: ", self.allJobsListSLURM)
            print("self.UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)
            print("self.CancelledJobs: ", self.CancelledJobs)

            if self.allJobsListSLURM[j] not in self.UniqueJobTrackListSLURM:
                print("here2.21")
                if self.allJobsListSLURM[j][0] in UniqueJobIDList:
                    print("here2.3")
                    # if the Job info is not unique but the Job ID is already stored, then the STATE or TIME of the job has updated (IDLE -> RUNNING)
                    # therefore we must update the entry in the UniqueJobTrackListSLURM
                    print("self.UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)
                    print("self.allJobsListSLURM[j]: ", self.allJobsListSLURM[j])
                    for i in self.UniqueJobTrackListSLURM:
                        if i[0] == self.allJobsListSLURM[j][0]:

                            if i[2] != self.allJobsListSLURM[j][2]:
                                #if the STATUS is different than that alread stored then update and print that
                                # job is now running and the current time
                                #update the STATE of the Job
                                i[2] = self.allJobsListSLURM[j][2]
                                ExecuteTime = datetime.now()
                                current_time = ExecuteTime.strftime("%H:%M:%S")
                                print('Job: ' + self.allJobsListSLURM[j][0] + ' started running at ' + current_time)
                                self.Messages.append(
                                    'Job: ' + self.allJobsListSLURM[j][0] + ' started running at ' + current_time)
                                print("here2.4")


                            #update the TIME RUNNING of the Job
                            #This should be done at every update!
                            i[1] = self.allJobsListSLURM[j][1]

                    print("here2.32")

                else:
                    print("here2.33")
                    # if the Job info isnt unique and the JobID isnt stored, then append to the Unique list
                    self.UniqueJobTrackListSLURM.append(self.allJobsListSLURM[j])
                    # The job has been added to the Unique list, so keep check if a Finished Email has been sent yet
                    self.EmailBeenSentCheckSLURM.append([])
                    self.EmailBeenSentCheckSLURM[j].append(self.UniqueJobTrackListSLURM[j][0])
                    self.EmailBeenSentCheckSLURM[j].append(0)
                    # We also need to add the job to the Cancel Combo Box so it can be canceled
                    self.CancelSLURMCB.addItem(self.UniqueJobTrackListSLURM[j][0])
                    print("here2.5")

        print("here3.0")
        #print("self.UniqueJobTrackListSLURM[j][0]: ", self.UniqueJobTrackListSLURM[j][0])
        print("self.CancelledJobs: ", self.CancelledJobs)
        for k in self.UniqueJobTrackListSLURM:
            if k[0] in self.CancelledJobs:
                # check if the job has been cancelled by user
                k[2] = 'Cancelled'
                print("JOB ", k[0], " HAS BEEN CANCELLED")
        print("UniqueJobTrackListSLURM: ", self.UniqueJobTrackListSLURM)

        print("here3")

        currentJobIDList = []
        for i in self.allJobsListSLURM:
            currentJobIDList.append(i[0])
        print("Current Job list: ", currentJobIDList)

        print("here4")

        # Now output the updated Unique Job info into the GUI Table

        # Update the Logfile at the end of each Table Update
        if self.ScriptsObj != None:
            LogFile = open(self.ScriptsObj.FileDirc + '\\' + self.ScriptsObj.cfg['CST File'][:-4] + '_Log.txt', "w")
            LogFile.write(self.Messages.toPlainText())
            LogFile.close()
        print("here8")

        self.CodeBusyCheck = 0

    def PrintJobTable(self):
        # Print Condor (LXPLUS) Jobs
        for i in range(len(self.UniqueJobTrackListCondor)):
            for j in range(len(self.UniqueJobTrackListCondor[i])):
                self.JobTable.setItem(i, j, QTableWidgetItem(self.UniqueJobTrackListCondor[i][j]))
        print("Updated Condor Table!")
        # Print SLURM Jobs
        for i in range(len(self.UniqueJobTrackListSLURM)):
            for j in range(len(self.UniqueJobTrackListSLURM[i])):
                #print("i", i)
                #print("j", j)
                self.JobTable.setItem(i + len(self.UniqueJobTrackListCondor), j, QTableWidgetItem(self.UniqueJobTrackListSLURM[i][j]))
        print("Updated SLURM Table!")

        # Here we will check if any of the Jobs are finished and will send the User an email to tell them
        # First need to check if an email has already been sent for each job

        print("Before self.EmailBeenSentCheckCondor: ", self.EmailBeenSentCheckCondor)
        print("Before self.EmailBeenSentCheckSLURM: ", self.EmailBeenSentCheckSLURM)
        print("self.UniqueJobTrackListCondor: ", self.UniqueJobTrackListCondor)
        print("self.UserEmail: ",self.UserEmail)
        if self.UserEmail != '':
            print("here10")
            for i in range(len(self.UniqueJobTrackListCondor)):
                print(self.UniqueJobTrackListCondor[i][2], self.EmailBeenSentCheckCondor[i][1])
                if self.UniqueJobTrackListCondor[i][2] == 'Finished' and self.EmailBeenSentCheckCondor[i][1] == 0:
                    EmailObj = EmailSender.Emailer(self.UserEmail)
                    print("here11")
                    EmailObj.SendMessage("This message has been sent from the LXPLUS Python GUI.","Your Job " + self.UniqueJobTrackListCondor[i][0] + " has finished on the LXPLUS/CONDOR server.")
                    # This will stop the email from being sent again in the next checks
                    print("Email sent for Job: ", self.UniqueJobTrackListCondor[i])
                    self.EmailBeenSentCheckCondor[i][1] = 1

            for i in range(len(self.UniqueJobTrackListSLURM)):
                if self.UniqueJobTrackListSLURM[i][2] == 'Finished' and self.EmailBeenSentCheckSLURM[i][1] == 0:
                    EmailObj = EmailSender.Emailer(self.UserEmail)
                    EmailObj.SendMessage("This message has been sent from the LXPLUS Python GUI.","Your Job " + self.UniqueJobTrackListSLURM[i][0] + " has finished on the SLURM server.")
                    # This will stop the email from being sent again in the next checks
                    print("Email sent for Job: ", self.UniqueJobTrackListSLURM[i])
                    self.EmailBeenSentCheckSLURM[i][1] = 1

        print("After self.EmailBeenSentCheckCondor: ", self.EmailBeenSentCheckCondor)
        print("After self.EmailBeenSentCheckSLURM: ", self.EmailBeenSentCheckSLURM)

    def UpdateJobTableTimer(self):
        # The code cannot constantly query the Jobs on each cluster due to the response times of the SSH output
        # Also, if the code is constantly doing something, this makes it more likely that the GUI will crash if the user
        # tries to do something else (move window, submit another job, etc).
        # Therefore we put the Update functions on a timer, i.e. there is some space between checks.
        # We also define a boolean function which is checked by the other functions, and defines whether the code is
        # currently working on updating the Job Table or not. If the code is updating the Table, then the other functions
        # will wait until the Table has finished updating.
        # Theoretically this could be done via multiple threads but this becomes complicated quickly.
        # This could be implimented sometime in the future but this works for the time being and is the safer option
        print("UpdateJobTableTimer() starting")
        self.TableUpdateStarted = 1
        # make initial empty list of job IDS (unique) to keep track of things
        self.JobIDTrackListCondor = []
        self.UniqueJobTrackListCondor = []
        self.JobIDTrackListSLURM = []
        self.UniqueJobTrackListSLURM = []
        time.sleep(5)
        while True:
            # Check the LXPLUS Jobs
            self.UpdateJobTableCondor()
            # Check the SLURM Jobs
            self.UpdateJobTableSLURM()
            # Output the found Jobs to the Table
            self.PrintJobTable()
            # Signal that the Table has been updated for the first time
            self.First_Update_Check = 1
            time.sleep(30)



app = QApplication([])
window = Window()
window.show()
app.exec()