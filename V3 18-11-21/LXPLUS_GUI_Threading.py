import os

from PyQt6.QtWidgets import QLabel, QApplication, QWidget, QPushButton, QGridLayout, QFileDialog, QGroupBox, QHBoxLayout, QVBoxLayout
import sys
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QLineEdit, QTextBrowser, QComboBox, QErrorMessage, QMainWindow
from PyQt6 import QtCore
from PyQt6.QtCore import QRunnable, pyqtSlot, QThreadPool
import Make_Scripts, OpenPutty, time

class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''
        self.fn(*self.args, **self.kwargs)

class Window(QWidget):

    def __init__(self):
        QWidget.__init__(self)
        self.SetupFileName = ''
        self.CSTFileName = ''
        self.loginCheck = 0
        self.ClusterID = ''
        self.JobFinishedCheck = 0
        self.setWindowTitle('LXPLUS CST Submission')
        self.setWindowIcon(QIcon("CERN-Logo.png"))
        #self.setFixedHeight(200)
        #self.setFixedWidth(200)


        self.setGeometry(500,300,400,400)
        self.UserPassArea()
        self.SelectFileArea()
        self.MessagesArea()
        self.ActionsArea()
        self.OptionsArea()

        #btn1 = QPushButton("Select File")
        #btn2 = QPushButton("Button 2")

        self.gridAll = QGridLayout()
        self.gridAll.addWidget(self.UserPassBox,0,0)
        self.gridAll.addWidget(self.SelectFileBox, 0, 1)
        self.gridAll.addWidget(self.MessagesBox, 1, 0,1,2)
        self.gridAll.addWidget(self.ActionsBox, 2, 0)
        self.gridAll.addWidget(self.OptionsBox, 1, 2)
        self.gridAll.setRowStretch(3, 1)
        self.gridAll.setColumnStretch(3, 1)



        self.setLayout(self.gridAll)





    def UserPassArea(self):
        self.UserPassBox = QGroupBox("Enter Username and Password")
        self.UserPassBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        userlabel = QLabel('Username:')
        pwlabel = QLabel('Password:')
        self.useredit = QLineEdit()
        self.passedit = QLineEdit()
        self.passedit.setEchoMode(QLineEdit.EchoMode.Password)

        UPgrid = QGridLayout()
        UPgrid.addWidget(userlabel, 0, 0)
        UPgrid.addWidget(self.useredit, 0, 1)
        UPgrid.addWidget(pwlabel, 1, 0)
        UPgrid.addWidget(self.passedit, 1, 1)

        UserLoginBtn = QPushButton('Login')
        UserLoginBtn.clicked.connect(self.UserAreaLogin)
        UPgrid.addWidget(UserLoginBtn, 1, 2)







        self.UserPassBox.setLayout(UPgrid)

    def UserAreaLogin(self):
        self.username = self.useredit.text()
        self.password = self.passedit.text()
        self.Messages.append('Login detailed entered (Make sure they are correct!).')
        self.loginCheck = 1




    def SelectFileArea(self):
        self.SelectFileBox = QGroupBox("Select Files:")
        self.SelectFileBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.SetupFileDialogButton = QPushButton('Setup File')
        self.SetupFileDialogButton.clicked.connect(self.SelectSetupFileButtonClick)

        self.CSTFileDialogButton = QPushButton('CST File')
        self.CSTFileDialogButton.clicked.connect(self.SelectCSTFileButtonClick)

        self.SetupFileNameLabel = QLabel()
        self.CSTFileNameLabel = QLabel()

        SFgrid = QGridLayout()

        SFgrid.addWidget(self.SetupFileDialogButton, 0,0)
        SFgrid.addWidget(self.CSTFileDialogButton, 1, 0)
        SFgrid.addWidget(self.SetupFileNameLabel, 0, 1)
        SFgrid.addWidget(self.CSTFileNameLabel, 1, 1)

        self.SelectFileBox.setLayout(SFgrid)

    def SelectSetupFileButtonClick(self):
        self.SetupFileName = QFileDialog.getOpenFileName(parent=self, directory=os.getcwd(),filter='*.yaml')[0]
        self.SetupFileNameLabel.setText('Setup file: ' + self.SetupFileName)
        print(self.SetupFileName)

    def SelectCSTFileButtonClick(self):
        self.CSTFileName = QFileDialog.getOpenFileName(parent=self, directory=os.getcwd(),filter='*.cst')[0]
        self.CSTFileNameLabel.setText('CST file: ' + self.CSTFileName)
        print(self.CSTFileName)

    def MessagesArea(self):
        self.MessagesBox = QGroupBox("Messages")
        self.SelectFileBox.setGeometry(QtCore.QRect(20, 30, 211, 91))
        self.Messages = QTextBrowser(self.MessagesBox)
        self.Messages.setGeometry(QtCore.QRect(10, 90, 331, 111))
        MessGrid= QGridLayout()
        MessGrid.addWidget(self.Messages,0,0)
        self.MessagesBox.setLayout(MessGrid)

    def ActionsArea(self):
        self.ActionsBox = QGroupBox('Actions')
        self.ActionsBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.SubmitButton = QPushButton('Submit Job')
        self.CancelButton = QPushButton('Cancel Job')
        self.MakeScriptsOnlyButton = QPushButton('Make Scripts only')

        self.SubmitButton.clicked.connect(self.SubmitButtonClick)
        self.MakeScriptsOnlyButton.clicked.connect(self.MakeScriptsOnlyButtonClick)
        self.CancelButton.clicked.connect(self.CancelButtonClick)

        ActionsGrid = QGridLayout()
        ActionsGrid.addWidget(self.SubmitButton, 0,0)
        ActionsGrid.addWidget(self.CancelButton, 0, 1)
        ActionsGrid.addWidget(self.MakeScriptsOnlyButton, 0, 2)

        self.ActionsBox.setLayout(ActionsGrid)

    def MakeScriptsOnlyButtonClick(self):
        if self.SetupFileName == '':
           print(self.SetupFileName)
           error_dialog = QErrorMessage(self)
           error_dialog.setWindowTitle("Error!")
           error_dialog.showMessage('Error: Setup file not selected!')
        else:
            print('Making Scripts')
            print(self.SetupFileName)
            print(self.SetupFileName.replace('/','\\'))
            self.ScriptsObj = Make_Scripts.scripts(self.SetupFileName.replace('/','\\'))

            if self.CSTCB.currentIndex() == 0:
                self.cstvers = 'cst2020'
            elif self.CSTCB.currentIndex() == 1:
                self.cstvers = 'cst2021'
            self.ScriptsObj.loadyaml()
            self.ScriptsObj.MakeSubFile(self.cstvers)
            self.ScriptsObj.MakeCSTScrpt(self.cstvers)
            self.Messages.append('Scripts have been created in ' + self.ScriptsObj.FileDirc)

    def SubmitExecute(self):
        # Here are the details that LXPLUX needs to sign in and connect
        host = "lxplus"
        port = 22
        self.initial = self.username[0]

        if self.CSTCB.currentIndex() == 0:
            self.cstvers = 'cst2020'
        elif self.CSTCB.currentIndex() == 1:
            self.cstvers = 'cst2021'

        self.ScriptsObj = Make_Scripts.scripts(self.SetupFileName.replace('/', '\\'))
        self.ScriptsObj.loadyaml()
        self.ScriptsObj.MakeSubFile(self.cstvers)
        self.ScriptsObj.MakeCSTScrpt(self.cstvers)

        self.connection = OpenPutty.ssh(host, self.username, self.password, self.ScriptsObj)

        self.connection.execute_scripts()

        # time.sleep(10)
        # print(strdata)    # print the last line of received data
        # print('==========================')
        # print(fulldata)   # This contains the complete data received.
        print('==========================')
        self.Messages.append('==========================')
        print("Waiting to be submitted...")
        self.Messages.append("Connecting to server on ip: " + str(host) + ".")
        self.Messages.append("Waiting to be submitted...")
        while "submitted to cluster" not in self.connection.strdata.split("\\n")[-2]:
            time.sleep(1)
        print("Job submitted!")
        self.Messages.append('Job submitted!')

        self.ClusterID = self.connection.strdata.split("\\n")[-2].split()[-1].split(".")[0]

        print("ClusterID: ", self.ClusterID)
        self.Messages.append('ClusterID: ' + self.ClusterID)
        self.command2 = "condor_wait -status log/cst." + self.ClusterID + ".log"
        self.connection.send_shell(self.command2)
        print('Waiting for Job to finish...')
        self.Messages.append('Waiting for Job to finish...')

        # while self.connection.askinput():
        #    if "All jobs done" in self.connection.strdata.split("\\n")[-2]:
        #        break
        #    pass

        while not self.JobFinishedCheck:
            time.sleep(5)

            if "All jobs done" in self.connection.strdata.split("\\n")[-2]:
                print("Job Finished!")
                self.Messages.append('Job Finished!')
                self.connection.close_connection()
                self.JobFinishedCheck = 1



    def SubmitButtonClick(self):
        if self.SetupFileName == '':
           print(self.SetupFileName)
           error_dialog = QErrorMessage(self)
           error_dialog.setWindowTitle("Error!")
           error_dialog.showMessage('Error: Setup file not selected!')
        elif self.CSTFileName == '':
            error_dialog = QErrorMessage(self)
            error_dialog.setWindowTitle("Error!")
            error_dialog.showMessage('Error: CST file not selected!')
        elif self.loginCheck == 0:
            error_dialog = QErrorMessage(self)
            error_dialog.setWindowTitle("Error!")
            error_dialog.showMessage('Error: Login details not entered!')
        else:
            self.Messages.append('Good to go!')
            worker = Worker(self.SubmitExecute)
            self.threadpool = QThreadPool()
            self.threadpool.start(worker)

    def OptionsArea(self):
        self.OptionsBox = QGroupBox('Options')
        self.OptionsBox.setGeometry(QtCore.QRect(20, 30, 211, 91))

        self.CSTCBLabel = QLabel("CST Version:")
        self.CSTCB = QComboBox()
        self.CSTCB.addItem("CST2020")
        self.CSTCB.addItem("CST2021")

        print(self.CSTCB.currentIndex())
        self.CSTCB.currentIndexChanged.connect(self.CSTSelection)

        OptionsGrid = QGridLayout()
        OptionsGrid.addWidget(self.CSTCBLabel,0,0)
        OptionsGrid.addWidget(self.CSTCB,0,1)


        self.OptionsBox.setLayout(OptionsGrid)

    def CSTSelection(self):
        print(self.CSTCB.currentIndex())

    def CancelButtonClick(self):
        self.command3 = "condor_rm " + self.ClusterID
        self.connection.send_shell("\x03")
        time.sleep(1)
        self.connection.send_shell(self.command3)
        print("command " + self.command3 + " has been sent.")
        while "removal" not in self.connection.strdata.split("\\n")[-2]:
            pass
        print("Job with ClusterID " + self.ClusterID + " has been marked for removal.")
        self.Messages.append("Job with ClusterID " + self.ClusterID + " has been marked for removal.")




app = QApplication([])
window = Window()
window.show()
app.exec()