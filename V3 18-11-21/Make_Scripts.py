import yaml

class scripts:
    FileDirc = ''
    yamlDirc = ''
    SubFile = None
    cfg = None
    SubFileName = ''
    CSTOption = ''
    RunScript = None
    RunScriptName = ''
    def __init__(self, yamlDirc1):
        self.yamlDirc = yamlDirc1
        #self.yamlDirc = r"C:\Users\mssulliv\cernbox\Documents\HTCondor_Test_files\Setup.yaml"
        yamlDircArray = self.yamlDirc.split("\\")
        yamlDircArray.pop()
        self.FileDirc = ''
        for i in yamlDircArray:
            self.FileDirc += i + "\\"


    def loadyaml(self):
        with open(self.yamlDirc, 'r') as ymlfile:
            self.cfg = yaml.safe_load(ymlfile)
        print("yaml file loaded.")

    def MakeSubFile(self, cstvers):
        self.SubFile = ['output = output/cst.$(ClusterID).out',
                   'error = error/cst.$(ClusterID).err',
                   'log = log/cst.$(ClusterID).log',
                   'environment = CST_INSTALLPATH=\"/afs/cern.ch/project/parc/' + cstvers + '\"; CST_WAIT_FOR_LICENSE=on; CST_LICENSE_SERVER=\"1705@licencst\";HOME=\"' +
                   self.cfg['AFS Dir'],
                   'executable  = ' + self.cfg['CST File'][:-4] + '_Script.sh',
                   'RequestCpus = ' + str(self.cfg['NProc']),
                   '+WCKey = CST',
                   'queue']
        # write the SubFile
        self.SubFileName = self.cfg['CST File'][:-4] + '_Sub'
        with open(self.FileDirc + self.cfg['CST File'][:-4] + '_Sub', 'w') as file:
            for i in self.SubFile:
                file.write('%s\n' % i)
        print("Submit file created in " + self.FileDirc)

    def MakeCSTScrpt(self,cstvers):
        # More solvers can be added if necessary! This is not exhaustive

        if self.cfg['Solver'] == 'Eigenmode':
            self.CSTOption = '-m -e '
        elif self.cfg['Solver'] == 'Wakefield':
            self.CSTOption = '-t -tw '
        elif self.cfg['Solver'] == 'TimeDomain':
            self.CSTOption = '-t -r '
        elif self.cfg['Solver'] == 'FrequencyDomain':
            self.CSTOption = '-m -f '
        elif self.cfg['Solver'] == 'Schematic':
            self.CSTOption = '-c '

        if self.cfg['Parametric'] == 'Yes':
            self.CSTOption += '-p '
        if self.cfg['Optimiser'] == 'Yes':
            self.CSTOption += '-o '

        # create the CSTScript .sh file
        self.RunScript = ['#!/bin/bash',
                     'export EOS_MGM_URL=root://eosuser.cern.ch',
                     'export HOME=' + self.cfg['AFS Dir'],
                     'eos cp ' + self.cfg['EOS Dir'] + self.cfg['CST File'] + ' ./' + self.cfg['CST File'],
                     '/afs/cern.ch/project/parc/' + cstvers + '/cst_design_environment ' + self.CSTOption + './' + self.cfg['CST File'],
                     'tar -cvf ./' + self.cfg['CST File'][:-4] + '.tar ./' + self.cfg['CST File'][:-4] + ' ./' + self.cfg['CST File'][
                                                                                                       :-4] + '.cst',
                     'eos cp ./' + self.cfg['CST File'][:-4] + '.tar ' + self.cfg['EOS Res'] + self.cfg['CST File'][:-4] + '.tar',
                     'exit 0'
                     ]
        # write the .sh file
        self.RunScriptName = self.cfg['CST File'][:-4] + '_Script.sh'
        with open(self.FileDirc + self.cfg['CST File'][:-4] + '_Script.sh', 'w') as file:
            for i in self.RunScript:
                file.write('%s\n' % i)
        print("CST Script file created in " + self.FileDirc)


