# LXPLUS Python

Python scripts and example files to automate submitting CST simulations to the LXPLUS cluster from a YAML setup file.
This project may be developed and extended to involve more functionality (plotting results, more CST options, GUI, etc.)

**NOTE**: Python script will need to be editted to include your own initial, username and password in order to login to the LXPLUS cluster!
**NOTE**: The example files Collimator_Sub and Collimator_Script would also need to be editted with your own username and initial for them to work. Alternatively, you could also create the submission and CST script files using the Python script with your own Setup.yaml file.

**Modules needed to run this project**

Paramiko: https://www.paramiko.org/
pyyaml: https://pyyaml.org/wiki/PyYAMLDocumentation
You dont need these if you are running the complied .exe files

**Prerequisites**

In order to work with Linux clusters, you need to have access to LXPLUS:
https://resources.web.cern.ch/resources/Manage/ListServices.aspx

You need to make sure that you are subscribed to EOS/CERNBox, LXPLUS, and AFS.


**VISUAL GUIDE TO VERSION 14**

When first opening the .exe file, the GUI will look like this:

![image-1.png](./image-1.png)

To login to the LXPLUS/SLURM clusters, enter your CERN details into the Username/Password boxes and click "login". Optionally, you may tick the "Remember my login details" box, and the GUI will save a text file which is local to your computer with your login details. These will be automatically filled in upon next opening the program to save you having to type them in every time. 

![image-2.png](./image-2.png)

If you have put in the correct details and you have the necessary access to LXPLUS/SLURM clusters, then you should be shown that you are logged in, as shown below. If only one of these boxes turns green but not the other one, then you should make sure that you have the correct permissions and that your account is correctly set up (AFS, EOS, etc).

![image-3.png](./image-3.png)

When submitting simulations, prepare the necessary Setup .yaml files (templates are available in the Repo) and drag and drop them into the box shown below. NOTE: the Setup .yaml files are NOT the same for LXPLUS and SLURM. Also, make sure that your Setup file is named correctly as described in the GUI: Setup_XXX_LXPLUS.yaml or Setup_XXX_SLURM.yaml for the LXPLUS and SLURM clusters respectively. The GUI will read the filename and send the simulation to the appropriate server. 

![image-4.png](./image-4.png)

Once dropped in the box, its file location will be shown. If this is correct, them click on "Submit Job(s)". You can drag and drop multiple files into the box and it will submit them systematically. Once the program has started submitting jobs, you may submit additional jobs into the box and the program will autodetect them and submit them once the GUI has finished checking on previously submitted simuations.

![image-5.png](./image-5.png)

Once the GUI has successfully sent the simualtion file to the cluster, it will be shown in the "Submitted Jobs" box.

![image-6.png](./image-6.png)

Once Jobs have been submitted, the GUI will automatically and periodically update the Job Table in a separate window. This gives infomation about the jobs that are currently running on the LXPLUS and SLURM clusters. Note, this information is only dependant on the available information given by the clusters themselves. Once a particular job is finished, the "State" of the job should turn into "Finished". However, it is recommended to use the "Notification: Always" line in the Setup .yaml file, as this will send an email to your CERN email once the Job has completed either successfully or if it has been abnormally aborted by the server.

![image-7.png](./image-7.png)

**Version 3** : now includes a GUI which can be used without the installation of extra Python modules!

The GUI requires you to enter your username and password (press login to enter the data), then requires you to select your setup.yaml and your .cst simulation file. You may also select which verion of CST you want to simulate the file.
Once everything is selected, press 'Submit Job' and the Messages box should alert you which stage the submission is at. You may also cancel the job if you need to!
If you want, you may also just create the script and submission files by selecting the setup.yaml file and pressing 'Make Scripts Only'

If the executable crashes at any point, just restart it and try again! However if it consistently crashes, let me know when it crashes and I will look into it, there may likely be some situations where I've not written a catch for certain errors.

If anybody has any ideas for helpful features to include, I will look into implimenting them depending on how straightforward it will be!

**Version 4**: 

now outputs more information in the messages box (some setup info, times of when the job was submitted and when it completed, as well as the time taken from submission -> completion). The program now also outputs a log file with all messages for later viewing.
You can now submit another job ONCE THE CURRENT JOB IS FINISHED. I will look into how to submit another job while there is a job currently running (possibly in another window? Could be solved by multithreading but it gets complex quickly!).
It may be possible to open 2 instances of the .exe window and run different CST jobs on each one, I am running tests on this now.

**Fixing the "NUMA Tensor" error on SLURM Cluster**

The answer seems to be to increase the number of CPUs you request (I got it working with 12 for a ~30 million meshcell simulation) as the amount of Memory that your job is assigned is connected to the number of CPUs that you request, i.e. the more CPUs you request, the more Memory you can use. This seems to fix the "NUMA Tensor" error that we've been getting. Unfortunately this means that you may have to wait a while before the simulation actually starts but there seems to be no way around that.

**Running jobs on SLURM Cluster**

It is now possible to use this GUI to submit jobs to the SLURM Cluster but to do this, you must have access to the SLURM cluster and have set it up correctly. In this section, I will give some information on how to do this from information given to me by the people who run the SLURM Cluster.

If you have a genuine reason to run on the SLURM Cluster (i.e. running huge simulations), you can request access here:
https://cern.service-now.com/service-portal/service-portal?id=kb_article&n=KB0003574&kb_category=1b93207b4f2e02404b4abc511310c755

Here is a exerpt from an email exchange I had with the SLURM people:

===========================================================================

I think that you should be able to run in the Slurm HPC cluster by doing the following 2 steps:

1. Make sure you have ssh keys configured in your hpc-batch account. The instructions should be identical to that of this Ansys KB: https://cern.service-now.com/service-portal/service-portal?id=kb_article&n=KB0006084 The part about Generate SSH keys)

2. Write a Slurm job submit file. There are some example Slurm submit files in the docs that you referenced. e.g. for a 4-node simulation that runs the Collimator.cst input file using CST 2020, with a 5 day time limit, using 1 task per node:

#!/bin/bash
#SBATCH -t 5:00:00
#SBATCH -N 4
#SBATCH -n 4
#SBATCH --exclusive

HOSTSFILE=.hostlist-job$SLURM_JOB_ID
srun hostname -f > $HOSTSFILE

if [ "$SLURM_PROCID" == "0" ]; then
  /afs/cern.ch/project/parc/cst2020/cst_design_environment --machinefile $HOSTSFILE -t -tw --withmpi Collimator.cst                                                                                                                    
  rm -f $HOSTSFILE
fi

And you can run it on a partition of the cluster using:
sbatch -p inf-short submit.sh


Since Aaron left, we have brought a newer cluster online (partition Photon) to which you can also submit using the equivalent command for the photon partition:
sbatch -p photon submit.sh

=============================================================================

I have attempted to run simulations on the photon partition but they failed due to a missing library file, however I have been told that they have installed the library file now so using the photon partition may be possible.


**Version 5** (15/12/2021)

The newest version of the GUI now allows CST simulations to be submitted to both the LXPLUS and SLURM clusters via setup ".yaml" files.
**NOTE:** the setup files for the LXPLUS and SLURM are different! I will upload templates for these differenct .yaml files.

The code now includes the following files:
EmailSender.py --> this defines a class which will send you an email when one of your simulations is finished! To do this, you must enter your email address in the "Options" box in the GUI before submitting a job or updating the Job Status Table.

Make_Scripts.py --> This takes the .yaml file for LXPLUS jobs and converts it into the format needed for the LXPLUS Cluster. This class also moves the script and .cst files to the local AFS directory assigned to you on the LXPUS Cluster.

Make_MPI_Script.py --> Same thing as Make_Scripts.py but for jobs submitted to the SLURM Cluster.

OpenPutty.py --> This class opens SSH shells to the LXPLUS and SLURM clusters to send the necessary commands to move files around and submit subs. This class is also used to query about the current running jobs on the clusters.

LXPUS_GUI_Threading --> This is the main file which imports the other .py files. This contains classes which define the GUI itself, as well as a class which allows threading of the code (to stop the the GUI from crashing while the underlying code does stuff!). This file is getting quite long now (~1000 lines) so I am intending to split this file up into multiple files for easier readibility in the future.

**Using the GUI**

When opening the program, you first need to enter your CERN username and password and click "login". This will then connect to the LXPLUS and SLURM cluster to check that your password is correct and also to request a new Kerboros token needed to submit jobs.
Enter your email address if you would like the program to email you when one of your jobs has completed. Leave this blank is you dont want an email! Note: the program must be open and running when the Job completes for this email to be sent.
You now have the option to save your last used username, password and email address to save time reentering them when you next open the program. These details are saved to a settings file in your local directory (Only you can see them!) upon clicking the "login" button. If you don't want your details saved, just leave the button unchecked.

In the "Select Files" box, clicking the "Setup File" and "CST File" buttons will allow you to select the setup .yaml file and .cst file required for the job you want to submit.

In the "Options" box, you can: 
1) select the CST version that you would like to use
2) Select the server that you would like to submit the job to: LXPLUS or SLURM.

Once logged in and your files have been selected, click "Submit Job" in the "Actions" box to submit your job. This will send your job to the selected cluster and then start to automatically update the Job Status Table to keep you informed on the current status of your Jobs. There is also extra information included like the Job ID, the date/time the Job was submitted, etc.

If you want to cancel a job on one of the clusters, select the Job ID in the "Cancel Jobs" box and click on the corresponding "Cancel Job" button.

It is possible to use the program to monitor your current jobs WITHOUT submitting any jobs. To do this, login using your CERN username and password and then click the "Update Table Only" button in the "Actions" box. You can still be sent emails upon Job completion in this mode, as long as your email address is entered before pressing the "Update Table Only" button.


**Troubleshooting**

There may be times there the program crashes for seemingly no reason but this is not too common. If this happens, try the following steps:

1) Once youve submitted a Job or Updated the Table, try not to click any other buttons or moving the program window straight away as the python GUI may not be able to handle this.
2) If the window stops responding, do not close it or click on it straight away as the underlying code may just be busy doing something, it normally starts responding again after a short amount of time.
3) If your Job doesnt submit correctly, check your setup .yaml and .cst files to see that everything is entered correctly (i.e. your directories are correct, etc.)
4) MAKE SURE that the correct serve is selected for what you want to do! It defaults to LXPLUS!
5) I will upload a .exe file both with and without the console, so you can look at the console for more information if something seems to be going wrong (I cant promise how helpful this will be!). However, if you have problems, send me a screenshot of the the GUI along with instructions on how to recreate the problem so I can look into it.
6) There will be some times where your Job will fail due to issues with the SLURM cluster itself. If you are able to, check the output.txt file in the returned failed CST files and see what it says (i.e. why if fails). If it doesnt seem like an obvious fix (Job timed out, etc) then you can submit a ticket to the CERN Service Portal and tell them that your Job failed. If you give them the Job ID and other details, then they can look into it from their end and try to work out why the Job failed.


