'''
Michael (CERN BE/RF-BR) 12-11-2019
This script creates the scripts to submit and run CST on the HTCondor Cluster
The script then runs the scripts through the LXPLUS Cluster through a SSH Tunnel
'''
import threading, paramiko, time, yaml



# This class controls the object which is used to control and send commands to the SSH terminal
# It can also read the output and show through the Python console
class ssh:

    shell = None
    client = None
    transport = None
    command1 = ''
    ScriptsObjlocal = None
    command2 = ''
    ClusterID = ''
    command3 = ''
    loginFail = 0
    password = ''
    username = ''

    def __init__(self, address, username, password, ScriptsObj):
        self.strdata = ''
        self.fulldata = ''
        self.password = password
        self.username = username
        self.ScriptsObjlocal = ScriptsObj
        print("Connecting to server on ip", str(address) + ".")
        self.client = paramiko.client.SSHClient()
        try:
            self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
            print("test0")
            self.client.connect(address, username=username, password=password, look_for_keys=False)
            print("test1")
            self.transport = paramiko.Transport((address, 22))

            self.transport.connect(username=username, password=password)
            print("test2")
        except paramiko.ssh_exception.AuthenticationException:
            print("Invalid Username/Password")
            self.loginFail = 1
            self.client.close()
            print("test3")
            return

        print("test3")
        print("Connected to server on ip", str(address) + ".")

        thread = threading.Thread(target=self.process)
        thread.daemon = True
        thread.start()

    def close_connection(self):
        if(self.client != None):
            self.client.close()
            self.transport.close()

    def open_shell(self):
        self.shell = self.client.invoke_shell()

    def send_shell(self, command):
        if(self.shell):
            self.shell.send(command + "\n")
        else:
            print("Shell not opened.")

    def process(self):
        #global strdata, self.fulldata
        while True:
            # Print data when available
            if self.shell is not None and self.shell.recv_ready():
                alldata = self.shell.recv(1024)
                while self.shell.recv_ready():
                    alldata += self.shell.recv(1024)
                self.strdata = self.strdata + str(alldata)
                self.fulldata = self.fulldata + str(alldata)
                self.strdata = self.print_lines(self.strdata) # print all received data except last line

    def print_lines(self, data):
        last_line = data
        if '\n' in data:
            lines = data.splitlines()
            for i in range(0, len(lines)-1):
                print(lines[i])
            last_line = lines[len(lines) - 1]
            if data.endswith('\n'):
                print(last_line)
                last_line = ''
        return last_line

    def execute_scripts(self):
        self.command1 = "cd " + self.ScriptsObjlocal.cfg['AFS Dir'] + " \n " + \
                   "rm " + self.ScriptsObjlocal.SubFileName + " " + self.ScriptsObjlocal.RunScriptName + " " + self.ScriptsObjlocal.cfg['CST File'] + " " + self.ScriptsObjlocal.cfg['CST File'][:-4] + '.tar' + " \n" + \
                   "cd " + self.ScriptsObjlocal.cfg['EOS Dir'] + "\n" + \
                   "cp " + self.ScriptsObjlocal.SubFileName + " " + self.ScriptsObjlocal.RunScriptName + " " + self.ScriptsObjlocal.cfg['CST File'] + " " + self.ScriptsObjlocal.cfg['AFS Dir'] + "\n" + \
                   "cd " + self.ScriptsObjlocal.cfg['AFS Dir'] + "\n" + \
                   "dos2unix " + self.ScriptsObjlocal.RunScriptName + "\n" + \
                   "condor_submit " + self.ScriptsObjlocal.SubFileName

        self.open_shell()
        self.send_shell(self.command1)

    def execute_scripts_SLURM(self):
        # Once logging into the HTC batch, you automatically get put in /hpcscratch/user/'username'
        # The script file will cp over the CST file so we only need to cp over the script
        self.command4 = "cd " + self.ScriptsObjlocal.FileDirc.replace(r"C:\Users\mssulliv\cernbox", '/eos/user/'+self.username[0]+'/'+self.username).replace("\\","/") + " \n " + \
                        "cp " + self.ScriptsObjlocal.RunScriptName + " " + "/hpcscratch/user/" + self.username + "/" + "\n" + \
                        "cd /hpcscratch/user/" + self.username + "/" + "\n" + \
                        "dos2unix " + self.ScriptsObjlocal.RunScriptName + "\n" + \
                        "sbatch " + self.ScriptsObjlocal.RunScriptName + "\n"

        #self.open_shell()
        self.send_shell(self.command4)
        print("self.command4: ", self.command4)
        print("execute_scripts_SLURM command sent")
        #self.close_connection()


    def askinput(self):
        # change to raw_input for python 2.7
        choice = input("\nOptions:\n1: Cancel Job\nelse: exit\ninput: ")
        if choice == "1":
            self.command3 = "condor_rm " + self.ClusterID
            self.send_shell("\x03")
            time.sleep(1)
            self.send_shell(self.command3)
            print("command " + self.command3 + " has been sent.")
            while "removal" not in self.strdata.split("\\n")[-2]:
                pass
            print("Job with ClusterID " + self.ClusterID + " has been marked for removal.")
        else:
            return 0
        return 1

# IT MAY BE NECESSARY TO GET THE KERBEROS TOKEN MANUALLY USING PUTTY
#"kinit " + username + "\n" + \
#            password + "\n" + \
